import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bangkokpattana/utils/format_locale.dart';
import 'package:bangkokpattana/utils/http_fetch_data.dart';
import 'package:bangkokpattana/utils/storage_preferences.dart';
import 'package:bangkokpattana/utils/storage_secure.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class BangkokpattanaApp extends StatefulWidget {
  const BangkokpattanaApp({super.key});

  @override
  State<BangkokpattanaApp> createState() => _BangkokpattanaApp();
}

class _BangkokpattanaApp extends State<BangkokpattanaApp> {
  Locale? currentLocale;
  String? currentPage;
  bool? loggedIn;

  @override
  void initState() {
    initApp();
    super.initState();
  }

  //* เรียกครั้งแรกเมื่อเข้าสู่ App
  Future<void> initApp() async {
    await initLocale();
    await initUser().then((userData) async {
      if (userData.isNotEmpty) {
        await initContent(userData);
        loggedIn = true;
      } else {
        loggedIn = true;
        return;
      }
      setState(() {});
    });
  }

  //* จัดการกับภาษาเริ่มต้น
  Future<void> initLocale() async {
    final String locale = await Preferences.readData(Type.string, 'LOCALE');

    // ถ้าไม่มีข้อมูลภาษา อยู่ใน Local storage ให้ดึงข้อมูลมาจาก Physical device
    if (locale.isEmpty) {
      currentLocale = LocaleFormat.stringToLocale(Platform.localeName);
    }

    // ถ้ามีข้อมูลภาษา ใน Local storage อยู่แล้ว ให้กำหนด Locale นั้น สำหรับใช้ใน App
    else {
      currentLocale = LocaleFormat.stringToLocale(locale);
    }
  }

  //* จัดการกับข้อมูลผู้ใช้เริ่มต้น
  Future<Map<String, dynamic>> initUser() async {
    final String? userId;
    final String? userDisplay;

    final bool rememberUser =
        await Preferences.readData(Type.bool, 'REMEMBER_ME');
    final username = await SecureStorage.readData('USERNAME');
    final password = await SecureStorage.readData('PASSWORD');
    final bool devMode = await Preferences.readData(Type.bool, 'DEV_MODE');

    // ถ้ามีการจดจำผู้ใช้ใน Local storage ให้ดึงข้อมูล User มาเก็บไว้ใน [loggedInUser]
    if (rememberUser && username != null && password != null) {
      //* ล็อคอินอีกครั้ง กรณี User/Pass มีการเปลี่ยนแปลง
      Map<String, dynamic> userData =
          await FetchData.getUser(username, password);

      //* Update ข้อมูล User กรณีมีการเปลี่ยนแปลง
      userId = userData['a'].toString();
      userDisplay = userData['b'];

      SecureStorage.writeData('USER_ID', userData['a'].toString());
      SecureStorage.writeData('USER_DISPLAY', userData['b']);

      return {
        'username': username,
        'password': password,
        'userId': userId,
        'userDisplay': userDisplay,
        'devMode': devMode,
      };
    }
    return {};
  }

  //* จัดการกับเนื้อหาเริ่มต้น
  Future<void> initContent(Map<String, dynamic> userData) async {
    if (userData.isNotEmpty) {
      /// อ่านข้อมูลของ Tab ที่เคยเปิดล่าสุดใน Feature page
      String page = await Preferences.readData(Type.string, 'CURRENT_FEATURE');

      /// ถ้าไม่มีข้อมูล [CURRENT_FEATURE] อยู่ใน Local storage
      /// กำหนดให้ [Dashboard] เป็นหน้าปัจจุบัน แล้วบันทึกไว้ใน Local storage
      if (page.isEmpty) {
        currentPage = 'dashboard';
        Preferences.writeData(Type.string, 'CURRENT_FEATURE', currentPage);
      } else {
        currentPage = page;
      }

      /// ตรวจสอบ Page ที่เคยเปิดก่อนหน้า แล้วดึงข้อมูลจาก Server เฉพาะส่วนนั้น เพื่อให้ไม่โหลดอีก
      //? ทำไมจึงต้องโหลดเฉพาะหน้าเดียวที่จะเข้าถึง ในเมื่อสามารถโหลดทุกหน้าตั้งแต่นี้
      //* เพราะจำเป็นต้องคำนึงถึงการใช้ Data ของผู้ใช้ที่ไม่จำเป็น (กรณีใช้อินเทอร์เน็ตตามปริมาณ)
      //* และผู้ใช้บางประเภทอาจดูเฉพาะหน้าเดียว และไม่ต้องการรอโหลดทุกหน้า
      switch (currentPage) {
        case 'dashboard':
        case 'tracking':
        case 'report':
        case 'notification':
        case 'settings':
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    initApp().then((value) {
      //* ถ้ามีการเข้าสู่ระบบแล้ว (จดจำผู้ใช้ในระบบ)
      //* จะเข้าสู่หน้า Feature แต่หากว่าไม่ใช่ จะไปยังหน้า [login]
      if (loggedIn == true) {
        print('1');
        context.goNamed(
          'Feature',
          queryParameters: {
            'page': currentPage,
          },
        );
      } else if (loggedIn == false) {
        print('2');
        context.goNamed('Login');
      }
    });

    //Todo: Add splash screen (App icon)
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
