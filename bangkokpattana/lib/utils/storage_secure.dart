import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  static const storage = FlutterSecureStorage();

  static Future<String?> readData(String key) async {
    return await storage.read(key: key);
  }

  static Future<Map<String, String>> readAllData() async {
    return await storage.readAll();
  }

  static Future<void> removeData(String key) async {
    storage.delete(key: key);
  }

  static Future<void> removeAllData(String key) async {
    storage.deleteAll();
  }

  static Future<void> writeData(String key, String value) async {
    storage.write(key: key, value: value);
  }
}
