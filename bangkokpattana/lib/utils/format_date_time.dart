class DateTimeFormat {
  /// แปลงรูปแบบวันที่ dd/MM/yyyy (ตัวอย่าง: 01/01/2023) เป็น [DateTime]
  static DateTime formattedToDateTime(String formatted) {
    final dateAndTime = formatted.split(' ');
    final date = dateAndTime[0].split('/') as List<int>;
    final time = dateAndTime[1].split(':') as List<int>..reversed;

    DateTime dateTime =
        DateTime(date[0], date[1], date[2], time[0], time[1], time[2]);

    return dateTime;
  }

  /// แปลงรูปแบบวันที่ dd/MM/yyyy (ตัวอย่าง: 01/01/2023) เป็น [DateTime]
  static int formattedToUnixTime(String formatted) {
    final dateAndTime = formatted.split(' ');

    final date = dateAndTime[0].split('/').map((e) => int.parse(e)).toList();
    final time = dateAndTime[1].split(':').map((e) => int.parse(e)).toList();

    DateTime dateTime =
        DateTime(date[2], date[1], date[0], time[0], time[1], time[2]);

    return dateTime.millisecondsSinceEpoch;
  }

  /// เปลี่ยน DateTime ให้เก็บค่าเฉพาะ วัน เดือน และปี
  static DateTime dateTimeToDate(DateTime dateTime) {
    DateTime date = DateTime(
      dateTime.year,
      dateTime.month,
      dateTime.day,
    );
    return date;
  }
}
