import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bangkokpattana/generated/l10n.dart';
import 'package:http/http.dart' as http;

enum FetchErrorType { login, server, timeout, internet, unexpected }

//* Exception ของ [FetchData] ใช้จัดการกับข้อมูลที่คาดว่า Error
class FetchDataException implements Exception {
  /// [type] เป็นประเภทของ Error ที่เกิดขึ้น
  FetchErrorType type;

  /// [title] เป็นชื่อของ Error ที่เกิดขึ้น
  /// ค่าที่ Return ออกมา อยู่ในรูปแบบ Localization ที่สามารถนำไปใช้ได้กับ App layer
  String title;

  /// [userMsg] เป็นข้อความ Error ที่เกิดขึ้น เพื่อแสดงให้ผู้ใช้เห็น
  /// ค่าที่ Return ออกมา อยู่ในรูปแบบ Localization ที่สามารถนำไปใช้ได้กับ App layer
  String userMsg;

  /// [secretMsg] เป็นข้อความ Error ที่เกิดขึ้น
  /// ใช้แสดงเฉพาะ Dev และผู้ที่อยู่เบื้องหลังของการพัฒนา App
  String secretMsg;

  FetchDataException(
    this.type,
    this.title,
    this.userMsg,
    this.secretMsg,
  );

  @override
  String toString() {
    return "FetchErrorException(title: $title, userMsg: $userMsg, secretMsg: $secretMsg)";
  }
}

class FetchData {
  //* ดึงข้อมูลผู้ใช้ ขณะที่ผู้ใช้ล็อคอิน
  static Future<Map<String, dynamic>> getUser(
      String username, String password) async {
    final client = http.Client();

    try {
      final uri = Uri.http(
          '203.151.215.230:9000',
          '/ezwmobileapi/api/User/IsAuthentication',
          {'UserName': username, 'UserPass': password});

      const userAuth = 'eZView';
      const passAuth = 'mb123';

      final String basicAuth =
          'Basic ${base64.encode(utf8.encode('$userAuth:$passAuth'))}';

      final response = await client
          .get(uri, headers: <String, String>{'authorization': basicAuth});

      final jsonResponse = jsonDecode(response.body);

      /// Status: OK
      if (response.statusCode == 200) {
        return jsonResponse;
      }

      /// Status: Not Found
      if (response.statusCode == 404) {
        throw FetchDataException(
          FetchErrorType.login,
          S.current.errorLoginFailTitle,
          S.current.errorLoginFailMsg,
          jsonResponse['b'],
        );
      }

      /// Status: Internal Server
      if (response.statusCode == 500) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }
    } on TimeoutException catch (e) {
      throw FetchDataException(
        FetchErrorType.timeout,
        S.current.errorTimeoutTitle,
        S.current.errorTimeoutMsg,
        e.message ?? '',
      );
    } on SocketException catch (e) {
      throw FetchDataException(
        FetchErrorType.internet,
        S.current.errorInternetTitle,
        S.current.errorInternetMessage,
        e.message,
      );
    } catch (e) {
      throw FetchDataException(
          FetchErrorType.unexpected,
          S.current.errorUnexpectedTitle,
          S.current.errorUnexpectedMsg,
          e.toString());
    }

    throw FetchDataException(
      FetchErrorType.unexpected,
      S.current.errorUnexpectedTitle,
      S.current.errorUnexpectedMsg,
      'Unexpected problem occurred while fetching User api',
    );
  }

  //* ดึงข้อมูล [DashboardPercentage]
  static Future<Map<String, dynamic>> getDbPercentage(String userId) async {
    try {
      final endpoint =
          'DashboardMobile/v1/DashboardMobile/$userId/DashboardPercentage';
      final path =
          'http://203.151.215.230:9000/bangkokpattanadashboardapi/api/$endpoint';

      final response = await http.get(Uri.parse(path));

      final jsonResponse = jsonDecode(response.body)[0];

      /// Status: OK
      if (response.statusCode == 200) {
        return jsonResponse;
      }

      /// Status: Not Found
      if (response.statusCode == 404) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }

      /// Status: Internal Server
      if (response.statusCode == 500) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }
    } on TimeoutException catch (e) {
      throw FetchDataException(
        FetchErrorType.timeout,
        S.current.errorTimeoutTitle,
        S.current.errorTimeoutMsg,
        e.message ?? '',
      );
    } on SocketException catch (e) {
      throw FetchDataException(
        FetchErrorType.internet,
        S.current.errorInternetTitle,
        S.current.errorInternetMessage,
        e.message,
      );
    } catch (e) {
      throw FetchDataException(
          FetchErrorType.unexpected,
          S.current.errorUnexpectedTitle,
          S.current.errorUnexpectedMsg,
          e.toString());
    }

    throw FetchDataException(
      FetchErrorType.unexpected,
      S.current.errorUnexpectedTitle,
      S.current.errorUnexpectedMsg,
      'Unexpected problem occurred while fetching DashboardPercentage api',
    );
  }

  //* ดึงข้อมูล [DashboardStatus]
  static Future<Map<String, dynamic>> getDbStatus(String userId) async {
    try {
      final endpoint =
          'DashboardMobile/v1/DashboardMobile/$userId/DashboardStatus';
      final path =
          'http://203.151.215.230:9000/bangkokpattanadashboardapi/api/$endpoint';

      final response = await http.get(Uri.parse(path));

      final jsonResponse = jsonDecode(response.body)[0];

      /// Status: OK
      if (response.statusCode == 200) {
        return jsonResponse;
      }

      /// Status: Not Found
      if (response.statusCode == 404) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }

      /// Status: Internal Server
      if (response.statusCode == 500) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }
    } on TimeoutException catch (e) {
      throw FetchDataException(
        FetchErrorType.timeout,
        S.current.errorTimeoutTitle,
        S.current.errorTimeoutMsg,
        e.message ?? '',
      );
    } on SocketException catch (e) {
      throw FetchDataException(
        FetchErrorType.internet,
        S.current.errorInternetTitle,
        S.current.errorInternetMessage,
        e.message,
      );
    } catch (e) {
      throw FetchDataException(
          FetchErrorType.unexpected,
          S.current.errorUnexpectedTitle,
          S.current.errorUnexpectedMsg,
          e.toString());
    }

    throw FetchDataException(
      FetchErrorType.unexpected,
      S.current.errorUnexpectedTitle,
      S.current.errorUnexpectedMsg,
      'Unexpected problem occurred while fetching DashboardStatus api',
    );
  }

  /* ดึงข้อมูล [VehicleByUser] */
  static Future<Map<String, dynamic>> getVehicleByUserId(String userId) async {
    try {
      final endpoint =
          'DashboardMobile/v1/DashboardMobile/$userId/DashboardStatus';
      final path =
          'http://203.151.215.230:9000/bangkokpattanadashboardapi/api/$endpoint';

      final response = await http.get(Uri.parse(path));

      final jsonResponse = jsonDecode(response.body)[0];

      /// Status: OK
      if (response.statusCode == 200) {
        return jsonResponse;
      }

      /// Status: Not Found
      if (response.statusCode == 404) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }

      /// Status: Internal Server
      if (response.statusCode == 500) {
        throw FetchDataException(
          FetchErrorType.server,
          S.current.errorServerTitle,
          S.current.errorServerMsg,
          jsonResponse['b'],
        );
      }
    } on TimeoutException catch (e) {
      throw FetchDataException(
        FetchErrorType.timeout,
        S.current.errorTimeoutTitle,
        S.current.errorTimeoutMsg,
        e.message ?? '',
      );
    } on SocketException catch (e) {
      throw FetchDataException(
        FetchErrorType.internet,
        S.current.errorInternetTitle,
        S.current.errorInternetMessage,
        e.message,
      );
    } catch (e) {
      throw FetchDataException(
          FetchErrorType.unexpected,
          S.current.errorUnexpectedTitle,
          S.current.errorUnexpectedMsg,
          e.toString());
    }

    throw FetchDataException(
      FetchErrorType.unexpected,
      S.current.errorUnexpectedTitle,
      S.current.errorUnexpectedMsg,
      'Unexpected problem occurred while fetching DashboardStatus api',
    );
  }
}
