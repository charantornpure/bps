import 'package:flutter/material.dart';

enum ViewportType { smallPhone, phone, largePhone, tablet }

class ViewportInfo {
  final BuildContext context;

  ViewportInfo(this.context);

  bool get landscapeMode {
    if (MediaQuery.of(context).size.width >
        MediaQuery.of(context).size.height) {
      return true;
    }
    return false;
  }

// ViewportType get viewportType {
//   double vWidth = MediaQuery.of(context).size.width;
//     double vHeight = MediaQuery.of(context).size.width;

//     if (vWidth )

// }
}
