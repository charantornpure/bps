import 'dart:ui';

class LocaleFormat {
  /// แปลงข้อมูล [Locale] เป็นรูปแแบบ [String]
  static localeToString(Locale locale) {
    String langCode = locale.languageCode;
    String? countryCode = locale.countryCode;

    return countryCode != null ? '${langCode}_$countryCode' : langCode;
  }

  /// แปลงข้อมูล [String] เป็นรูปแแบบ [Locale]
  static Locale stringToLocale(String locale) {
    List<String> splitLocale = locale.split('_');
    return Locale(splitLocale[0], splitLocale[1]);
  }
}
