import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

enum Type { int, bool, double, string }

class Preferences {
  static Future<void> writeData(Type type, String name, dynamic value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    switch (type) {
      case Type.int:
        await prefs.setInt(name, value);
        break;
      case Type.bool:
        await prefs.setBool(name, value);
        break;
      case Type.double:
        await prefs.setDouble(name, value);
        break;
      case Type.string:
        await prefs.setString(name, value);
    }
  }

  static Future<dynamic> readData(Type type, String name) async {
    final prefs = await SharedPreferences.getInstance();
    switch (type) {
      case Type.int:
        return prefs.getInt(name) ?? 0;
      case Type.bool:
        return prefs.getBool(name) ?? false;
      case Type.double:
        return prefs.getDouble(name) ?? 0.0;
      case Type.string:
        return prefs.getString(name) ?? "";
    }
  }

  static Future<void> removeData(name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(name);
  }
}
