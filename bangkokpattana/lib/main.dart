import 'package:bangkokpattana/app.dart';

import 'package:bangkokpattana/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:go_router/go_router.dart';

import 'package:bangkokpattana/styles/styles.dart';

//* Pages
import 'package:bangkokpattana/pages/features/feature.dart';
import 'package:bangkokpattana/pages/login/login.dart';

//* Bloc
import 'package:bangkokpattana/pages/login/bloc/login_bloc.dart';
import 'package:bangkokpattana/pages/features/dashboard/bloc/dashboard_bloc.dart';
import 'package:bangkokpattana/pages/features/settings/bloc/settings_bloc.dart';

import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        //* Status bar color for android
        statusBarColor: EzvColors.primary20,
        //* Status brightness for android
        statusBarIconBrightness: Brightness.light,
        // Status brightness for ios
        statusBarBrightness: Brightness.light,
      ),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<LoginBloc>(
            create: (context) => LoginBloc(),
          ),
          BlocProvider<DashboardBloc>(
            create: (context) => DashboardBloc(),
          ),
          BlocProvider<SettingsBloc>(
            create: (context) => SettingsBloc(),
          ),
        ],
        child: MaterialApp.router(
          theme: ThemeData(
            fontFamily: 'NotoSans',
            primaryColor: Colors.white,
          ),
          builder: EasyLoading.init(),
          debugShowCheckedModeBanner: false,
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: const <Locale>[
            Locale('th', 'TH'),
            Locale('en', 'US'),
          ],
          routerConfig: GoRouter(routes: [
            GoRoute(
              path: '/',
              builder: (context, state) => const BangkokpattanaApp(),
            ),
            GoRoute(
              path: '/login',
              name: 'Login',
              builder: (_, __) => const Login(),
            ),
            GoRoute(
              path: '/feature',
              name: 'Feature',
              builder: (context, state) {
                return Feature(
                  page: state.uri.queryParameters['page']!,
                );
              },
            ),
          ]),
        ),
      ),
    ),
  );
}
