// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a th locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'th';

  static String m0(date) => "${date}";

  static String m1(date) => "${date}";

  static String m2(time) => "${time}";

  static String m3(amount) => "รหัสผ่านควรมีมากกว่า ${amount} ตัวอักษร";

  static String m4(amount) => "ชื่อผู้ใช้ควรมีมากกว่า ${amount} ตัวอักษร";

  static String m5(volumn) =>
      "${Intl.plural(volumn, zero: '0 คัน', one: '1 คัน', other: '${volumn} คัน')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "btnCancel": MessageLookupByLibrary.simpleMessage("ปฏิเสธ"),
        "btnExit": MessageLookupByLibrary.simpleMessage("ออก"),
        "btnLogin": MessageLookupByLibrary.simpleMessage("เข้าสู่ระบบ"),
        "btnLoginAgain":
            MessageLookupByLibrary.simpleMessage("เข้าสู่ระบบอีกครั้ง"),
        "btnLoginLoading":
            MessageLookupByLibrary.simpleMessage("กำลังเข้าสู่ระบบ..."),
        "btnPolicyAccept":
            MessageLookupByLibrary.simpleMessage("ยอมรับข้อตกลง"),
        "btnPolicyDone":
            MessageLookupByLibrary.simpleMessage("ยอมรับข้อตกลงแล้ว"),
        "btnPolicyRead":
            MessageLookupByLibrary.simpleMessage("นโยบายความเป็นส่วนตัว"),
        "btnPolicyReadAgain": MessageLookupByLibrary.simpleMessage("อ่านต่อ"),
        "btnReportUs": MessageLookupByLibrary.simpleMessage("แจ้งปัญหา"),
        "capEngineOff": MessageLookupByLibrary.simpleMessage("จอดดับเครื่อง"),
        "capEngineOn": MessageLookupByLibrary.simpleMessage("เคลื่อนที่"),
        "capIdle": MessageLookupByLibrary.simpleMessage("จอดไม่ดับเครื่อง"),
        "capInSpot": MessageLookupByLibrary.simpleMessage("เข้าจุดจอด"),
        "capMove": MessageLookupByLibrary.simpleMessage("เคลื่อนที่"),
        "capNoStatus": MessageLookupByLibrary.simpleMessage("ไม่มีสถานะ"),
        "capNoUpdate": MessageLookupByLibrary.simpleMessage("ไม่อัปเดต"),
        "capNotTouchId": MessageLookupByLibrary.simpleMessage("ไม่รูดบัตร"),
        "capOverHighTemp":
            MessageLookupByLibrary.simpleMessage("อุณหภูมิสูงเกิน"),
        "capOverLowTemp":
            MessageLookupByLibrary.simpleMessage("อุณหภูมิต่ำเกิน"),
        "capOverSpeed": MessageLookupByLibrary.simpleMessage("ความเร็วเกิน"),
        "checkRememberMe":
            MessageLookupByLibrary.simpleMessage("จดจำผู้ใช้ในระบบ"),
        "dateTimeDM": m0,
        "dateTimeDMY": m1,
        "dateTimeHM": m2,
        "errorInternetMessage": MessageLookupByLibrary.simpleMessage(
            "จำเป็นต้องมีการเชื่อมต่ออินเทอร์เน็ต เพื่อใช้งาน \"bangkokpattana\" กรุณาเชื่อมต่ออินเทอร์เน็ต แล้วลองใหม่อีกครั้ง"),
        "errorInternetTitle": MessageLookupByLibrary.simpleMessage(
            "ไม่มีการเชื่อมต่ออินเทอร์เน็ต"),
        "errorLoginFailMsg": MessageLookupByLibrary.simpleMessage(
            "ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง กรุณากรอกข้อมูลใหม่อีกครั้ง หรือหากท่านพบปัญหาในการเข้าสู่ระบบ กรุณาแจ้งให้เราทราบ"),
        "errorLoginFailTitle":
            MessageLookupByLibrary.simpleMessage("การเข้าสู่ระบบล้มเหลว"),
        "errorServerMsg": MessageLookupByLibrary.simpleMessage(
            "ขออภัยในความไม่สะดวก เนื่องจากในขณะนี้เซิฟเวอร์เกิดการขัดข้อง และทางเรากำลังดำเนินการแก้ปัญหา กรุณาลองใหม่อีกครั้งในภายหลัง"),
        "errorServerTitle":
            MessageLookupByLibrary.simpleMessage("เซิร์ฟเวอร์เกิดการขัดข้อง"),
        "errorTimeoutMsg": MessageLookupByLibrary.simpleMessage(
            "การเชื่อมต่อใช้เวลานานเกินไป โปรดตรวจสอบอินเทอร์เน็ตของท่าน แล้วดำเนินการใหม่อีกครั้ง หากปัญหายังคงเกิดขึ้นกรุณาแจ้งให้เราทราบ"),
        "errorTimeoutTitle":
            MessageLookupByLibrary.simpleMessage("การเชื่อมต่อล้มเหลว"),
        "errorUnexpectedMsg": MessageLookupByLibrary.simpleMessage(
            "เกิดข้อผิดพลาดที่ไม่ได้คาดหมายขึ้น กรุณาแจ้งให้เราทราบ"),
        "errorUnexpectedTitle":
            MessageLookupByLibrary.simpleMessage("เกิดข้อผิดพลาด"),
        "holdPassword": MessageLookupByLibrary.simpleMessage("รหัสผ่าน"),
        "holdUsername": MessageLookupByLibrary.simpleMessage("ชื่อผู้ใช้งาน"),
        "invalidEmptyPsw":
            MessageLookupByLibrary.simpleMessage("กรุณากรอกรหัสผ่าน"),
        "invalidEmptyUser":
            MessageLookupByLibrary.simpleMessage("กรุณากรอกชื่อผู้ใช้งาน"),
        "invalidEmptyUserAndPsw": MessageLookupByLibrary.simpleMessage(
            "กรุณากรอกชื่อผู้ใช้ และรหัสผ่าน"),
        "invalidPswLessThan": m3,
        "invalidUserLessThan": m4,
        "navDashboard": MessageLookupByLibrary.simpleMessage("ภาพรวม"),
        "navReport": MessageLookupByLibrary.simpleMessage("รายงาน"),
        "navSettings": MessageLookupByLibrary.simpleMessage("การตั้งค่า"),
        "navTracking": MessageLookupByLibrary.simpleMessage("การติดตาม"),
        "pageTitleDashboard": MessageLookupByLibrary.simpleMessage("ภาพรวม"),
        "pageTitleReport": MessageLookupByLibrary.simpleMessage("รายงาน"),
        "pageTitleSettings": MessageLookupByLibrary.simpleMessage("การตั้งค่า"),
        "pageTitleTracking": MessageLookupByLibrary.simpleMessage("การติดตาม"),
        "textCarVolume": m5,
        "textToday": MessageLookupByLibrary.simpleMessage("วันนี้"),
        "textYesterday": MessageLookupByLibrary.simpleMessage("เมื่อวาน"),
        "titleDashboardChart":
            MessageLookupByLibrary.simpleMessage("สถานะรถย้อนหลัง 24 ชั่วโมง"),
        "titlePolicy":
            MessageLookupByLibrary.simpleMessage("นโยบายความเป็นส่วนตัว"),
        "titleTerms":
            MessageLookupByLibrary.simpleMessage("เงื่อนไขการให้บริการ"),
        "warnConsentDenyMsg": MessageLookupByLibrary.simpleMessage(
            "ผู้ใช้จำเป็นต้องยอมรับ \"นโยบายความเป็นส่วนตัว\" ก่อนดำเนินการเข้าสู่ระบบต่อไป"),
        "warnPolicyDenyTitle":
            MessageLookupByLibrary.simpleMessage("การเข้าสู่ระบบถูกปฏิเสธ")
      };
}
