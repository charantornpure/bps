// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(date) => "${date}";

  static String m1(date) => "${date}";

  static String m2(time) => "${time}";

  static String m3(amount) =>
      "Password should be more than ${amount} characters.";

  static String m4(amount) =>
      "Username should be more than ${amount} characters.";

  static String m5(volumn) =>
      "${Intl.plural(volumn, zero: 'No car', one: '1 car', other: '${volumn} cars')}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "btnCancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "btnExit": MessageLookupByLibrary.simpleMessage("ออก"),
        "btnLogin": MessageLookupByLibrary.simpleMessage("Login"),
        "btnLoginAgain": MessageLookupByLibrary.simpleMessage("Login Again"),
        "btnLoginLoading":
            MessageLookupByLibrary.simpleMessage("Logging in..."),
        "btnPolicyAccept":
            MessageLookupByLibrary.simpleMessage("Accept Privacy Policy"),
        "btnPolicyDone":
            MessageLookupByLibrary.simpleMessage("Accepted Policy"),
        "btnPolicyRead": MessageLookupByLibrary.simpleMessage("Privacy Policy"),
        "btnPolicyReadAgain":
            MessageLookupByLibrary.simpleMessage("Read Again"),
        "btnReportUs": MessageLookupByLibrary.simpleMessage("Report Us"),
        "capEngineOff": MessageLookupByLibrary.simpleMessage("Parked"),
        "capEngineOn": MessageLookupByLibrary.simpleMessage("Moving"),
        "capIdle": MessageLookupByLibrary.simpleMessage("Idling"),
        "capInSpot": MessageLookupByLibrary.simpleMessage("In spot"),
        "capMove": MessageLookupByLibrary.simpleMessage("Moving"),
        "capNoStatus": MessageLookupByLibrary.simpleMessage("No Status"),
        "capNoUpdate": MessageLookupByLibrary.simpleMessage("No Update"),
        "capNotTouchId": MessageLookupByLibrary.simpleMessage("No Authen"),
        "capOverHighTemp": MessageLookupByLibrary.simpleMessage("Over Heat"),
        "capOverLowTemp": MessageLookupByLibrary.simpleMessage("Over Cold"),
        "capOverSpeed": MessageLookupByLibrary.simpleMessage("Over Speed"),
        "checkRememberMe": MessageLookupByLibrary.simpleMessage("Remember Me"),
        "dateTimeDM": m0,
        "dateTimeDMY": m1,
        "dateTimeHM": m2,
        "errorInternetMessage": MessageLookupByLibrary.simpleMessage(
            "You need an internet connection. Please connect to the internet and try again."),
        "errorInternetTitle":
            MessageLookupByLibrary.simpleMessage("No internet Connection"),
        "errorLoginFailMsg": MessageLookupByLibrary.simpleMessage(
            "Username or password is incorrect, please fill in again. If you encounter any problems logging in, please report us."),
        "errorLoginFailTitle":
            MessageLookupByLibrary.simpleMessage("Login Failed"),
        "errorServerMsg": MessageLookupByLibrary.simpleMessage(
            "We apologize for the inconvenience. The server is temporarily unavailable due to maintenance. Please try again later."),
        "errorServerTitle":
            MessageLookupByLibrary.simpleMessage("Server Error"),
        "errorTimeoutMsg":
            MessageLookupByLibrary.simpleMessage("Connection Timeout"),
        "errorTimeoutTitle": MessageLookupByLibrary.simpleMessage(
            "The connection has timed out. Please check your internet and try again. If the problem still occurs please report us."),
        "errorUnexpectedMsg": MessageLookupByLibrary.simpleMessage(
            "An unexpected error has occurred. please report us"),
        "errorUnexpectedTitle":
            MessageLookupByLibrary.simpleMessage("Unexpected Error"),
        "holdPassword": MessageLookupByLibrary.simpleMessage("Password"),
        "holdUsername": MessageLookupByLibrary.simpleMessage("Username"),
        "invalidEmptyPsw":
            MessageLookupByLibrary.simpleMessage("Please enter password."),
        "invalidEmptyUser":
            MessageLookupByLibrary.simpleMessage("Please enter username."),
        "invalidEmptyUserAndPsw": MessageLookupByLibrary.simpleMessage(
            "Please enter username and password."),
        "invalidPswLessThan": m3,
        "invalidUserLessThan": m4,
        "navDashboard": MessageLookupByLibrary.simpleMessage("Dashboard"),
        "navReport": MessageLookupByLibrary.simpleMessage("Report"),
        "navSettings": MessageLookupByLibrary.simpleMessage("Settings"),
        "navTracking": MessageLookupByLibrary.simpleMessage("Tracking"),
        "pageTitleDashboard": MessageLookupByLibrary.simpleMessage("Dashboard"),
        "pageTitleReport": MessageLookupByLibrary.simpleMessage("Report"),
        "pageTitleSettings": MessageLookupByLibrary.simpleMessage("Settings"),
        "pageTitleTracking": MessageLookupByLibrary.simpleMessage("Tracking"),
        "textCarVolume": m5,
        "textToday": MessageLookupByLibrary.simpleMessage("Today"),
        "textYesterday": MessageLookupByLibrary.simpleMessage("Yesterday"),
        "titleDashboardChart": MessageLookupByLibrary.simpleMessage(
            "Vehicle Status for the past 24 hour"),
        "titlePolicy": MessageLookupByLibrary.simpleMessage("Privacy Policy"),
        "titleTerms": MessageLookupByLibrary.simpleMessage("Terms of Service"),
        "warnConsentDenyMsg": MessageLookupByLibrary.simpleMessage(
            "User need to accept the \"Privacy Policy\" before continuing login."),
        "warnPolicyDenyTitle":
            MessageLookupByLibrary.simpleMessage("Login is Denined")
      };
}
