// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Cancel`
  String get btnCancel {
    return Intl.message(
      'Cancel',
      name: 'btnCancel',
      desc: '',
      args: [],
    );
  }

  /// `ออก`
  String get btnExit {
    return Intl.message(
      'ออก',
      name: 'btnExit',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get btnLogin {
    return Intl.message(
      'Login',
      name: 'btnLogin',
      desc: '',
      args: [],
    );
  }

  /// `Login Again`
  String get btnLoginAgain {
    return Intl.message(
      'Login Again',
      name: 'btnLoginAgain',
      desc: '',
      args: [],
    );
  }

  /// `Logging in...`
  String get btnLoginLoading {
    return Intl.message(
      'Logging in...',
      name: 'btnLoginLoading',
      desc: '',
      args: [],
    );
  }

  /// `Accept Privacy Policy`
  String get btnPolicyAccept {
    return Intl.message(
      'Accept Privacy Policy',
      name: 'btnPolicyAccept',
      desc: '',
      args: [],
    );
  }

  /// `Accepted Policy`
  String get btnPolicyDone {
    return Intl.message(
      'Accepted Policy',
      name: 'btnPolicyDone',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get btnPolicyRead {
    return Intl.message(
      'Privacy Policy',
      name: 'btnPolicyRead',
      desc: '',
      args: [],
    );
  }

  /// `Read Again`
  String get btnPolicyReadAgain {
    return Intl.message(
      'Read Again',
      name: 'btnPolicyReadAgain',
      desc: '',
      args: [],
    );
  }

  /// `Report Us`
  String get btnReportUs {
    return Intl.message(
      'Report Us',
      name: 'btnReportUs',
      desc: '',
      args: [],
    );
  }

  /// `Parked`
  String get capEngineOff {
    return Intl.message(
      'Parked',
      name: 'capEngineOff',
      desc: '',
      args: [],
    );
  }

  /// `Moving`
  String get capEngineOn {
    return Intl.message(
      'Moving',
      name: 'capEngineOn',
      desc: '',
      args: [],
    );
  }

  /// `Idling`
  String get capIdle {
    return Intl.message(
      'Idling',
      name: 'capIdle',
      desc: '',
      args: [],
    );
  }

  /// `In spot`
  String get capInSpot {
    return Intl.message(
      'In spot',
      name: 'capInSpot',
      desc: '',
      args: [],
    );
  }

  /// `Moving`
  String get capMove {
    return Intl.message(
      'Moving',
      name: 'capMove',
      desc: '',
      args: [],
    );
  }

  /// `No Status`
  String get capNoStatus {
    return Intl.message(
      'No Status',
      name: 'capNoStatus',
      desc: '',
      args: [],
    );
  }

  /// `No Authen`
  String get capNotTouchId {
    return Intl.message(
      'No Authen',
      name: 'capNotTouchId',
      desc: '',
      args: [],
    );
  }

  /// `No Update`
  String get capNoUpdate {
    return Intl.message(
      'No Update',
      name: 'capNoUpdate',
      desc: '',
      args: [],
    );
  }

  /// `Over Heat`
  String get capOverHighTemp {
    return Intl.message(
      'Over Heat',
      name: 'capOverHighTemp',
      desc: '',
      args: [],
    );
  }

  /// `Over Cold`
  String get capOverLowTemp {
    return Intl.message(
      'Over Cold',
      name: 'capOverLowTemp',
      desc: '',
      args: [],
    );
  }

  /// `Over Speed`
  String get capOverSpeed {
    return Intl.message(
      'Over Speed',
      name: 'capOverSpeed',
      desc: '',
      args: [],
    );
  }

  /// `Remember Me`
  String get checkRememberMe {
    return Intl.message(
      'Remember Me',
      name: 'checkRememberMe',
      desc: '',
      args: [],
    );
  }

  /// `{date}`
  String dateTimeDM(DateTime date) {
    final DateFormat dateDateFormat =
        DateFormat('dd MMM', Intl.getCurrentLocale());
    final String dateString = dateDateFormat.format(date);

    return Intl.message(
      '$dateString',
      name: 'dateTimeDM',
      desc: '',
      args: [dateString],
    );
  }

  /// `{date}`
  String dateTimeDMY(DateTime date) {
    final DateFormat dateDateFormat =
        DateFormat('dd MMM y', Intl.getCurrentLocale());
    final String dateString = dateDateFormat.format(date);

    return Intl.message(
      '$dateString',
      name: 'dateTimeDMY',
      desc: '',
      args: [dateString],
    );
  }

  /// `{time}`
  String dateTimeHM(DateTime time) {
    final DateFormat timeDateFormat =
        DateFormat('HH:mm', Intl.getCurrentLocale());
    final String timeString = timeDateFormat.format(time);

    return Intl.message(
      '$timeString',
      name: 'dateTimeHM',
      desc: '',
      args: [timeString],
    );
  }

  /// `You need an internet connection. Please connect to the internet and try again.`
  String get errorInternetMessage {
    return Intl.message(
      'You need an internet connection. Please connect to the internet and try again.',
      name: 'errorInternetMessage',
      desc: '',
      args: [],
    );
  }

  /// `No internet Connection`
  String get errorInternetTitle {
    return Intl.message(
      'No internet Connection',
      name: 'errorInternetTitle',
      desc: '',
      args: [],
    );
  }

  /// `Username or password is incorrect, please fill in again. If you encounter any problems logging in, please report us.`
  String get errorLoginFailMsg {
    return Intl.message(
      'Username or password is incorrect, please fill in again. If you encounter any problems logging in, please report us.',
      name: 'errorLoginFailMsg',
      desc: '',
      args: [],
    );
  }

  /// `Login Failed`
  String get errorLoginFailTitle {
    return Intl.message(
      'Login Failed',
      name: 'errorLoginFailTitle',
      desc: '',
      args: [],
    );
  }

  /// `We apologize for the inconvenience. The server is temporarily unavailable due to maintenance. Please try again later.`
  String get errorServerMsg {
    return Intl.message(
      'We apologize for the inconvenience. The server is temporarily unavailable due to maintenance. Please try again later.',
      name: 'errorServerMsg',
      desc: '',
      args: [],
    );
  }

  /// `Server Error`
  String get errorServerTitle {
    return Intl.message(
      'Server Error',
      name: 'errorServerTitle',
      desc: '',
      args: [],
    );
  }

  /// `Connection Timeout`
  String get errorTimeoutMsg {
    return Intl.message(
      'Connection Timeout',
      name: 'errorTimeoutMsg',
      desc: '',
      args: [],
    );
  }

  /// `The connection has timed out. Please check your internet and try again. If the problem still occurs please report us.`
  String get errorTimeoutTitle {
    return Intl.message(
      'The connection has timed out. Please check your internet and try again. If the problem still occurs please report us.',
      name: 'errorTimeoutTitle',
      desc: '',
      args: [],
    );
  }

  /// `An unexpected error has occurred. please report us`
  String get errorUnexpectedMsg {
    return Intl.message(
      'An unexpected error has occurred. please report us',
      name: 'errorUnexpectedMsg',
      desc: '',
      args: [],
    );
  }

  /// `Unexpected Error`
  String get errorUnexpectedTitle {
    return Intl.message(
      'Unexpected Error',
      name: 'errorUnexpectedTitle',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get holdPassword {
    return Intl.message(
      'Password',
      name: 'holdPassword',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get holdUsername {
    return Intl.message(
      'Username',
      name: 'holdUsername',
      desc: '',
      args: [],
    );
  }

  /// `Please enter password.`
  String get invalidEmptyPsw {
    return Intl.message(
      'Please enter password.',
      name: 'invalidEmptyPsw',
      desc: '',
      args: [],
    );
  }

  /// `Please enter username.`
  String get invalidEmptyUser {
    return Intl.message(
      'Please enter username.',
      name: 'invalidEmptyUser',
      desc: '',
      args: [],
    );
  }

  /// `Please enter username and password.`
  String get invalidEmptyUserAndPsw {
    return Intl.message(
      'Please enter username and password.',
      name: 'invalidEmptyUserAndPsw',
      desc: '',
      args: [],
    );
  }

  /// `Password should be more than {amount} characters.`
  String invalidPswLessThan(int amount) {
    return Intl.message(
      'Password should be more than $amount characters.',
      name: 'invalidPswLessThan',
      desc: '',
      args: [amount],
    );
  }

  /// `Username should be more than {amount} characters.`
  String invalidUserLessThan(int amount) {
    return Intl.message(
      'Username should be more than $amount characters.',
      name: 'invalidUserLessThan',
      desc: '',
      args: [amount],
    );
  }

  /// `Dashboard`
  String get navDashboard {
    return Intl.message(
      'Home',
      name: 'navDashboard',
      desc: '',
      args: [],
    );
  }

  /// `Report`
  String get navReport {
    return Intl.message(
      'Report',
      name: 'navReport',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get navSettings {
    return Intl.message(
      'Settings',
      name: 'navSettings',
      desc: '',
      args: [],
    );
  }

  /// `Tracking`
  String get navTracking {
    return Intl.message(
      'Tracking',
      name: 'navTracking',
      desc: '',
      args: [],
    );
  }

  /// `Dashboard`
  String get pageTitleDashboard {
    return Intl.message(
      'Home',
      name: 'pageTitleDashboard',
      desc: '',
      args: [],
    );
  }

  /// `Report`
  String get pageTitleReport {
    return Intl.message(
      'Report',
      name: 'pageTitleReport',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get pageTitleSettings {
    return Intl.message(
      'Settings',
      name: 'pageTitleSettings',
      desc: '',
      args: [],
    );
  }

  /// `Tracking`
  String get pageTitleTracking {
    return Intl.message(
      'Tracking',
      name: 'pageTitleTracking',
      desc: '',
      args: [],
    );
  }

  /// `{volumn, plural, zero{No car} one{1 car} other{{volumn} cars}}`
  String textCarVolume(int volumn) {
    return Intl.plural(
      volumn,
      zero: 'No car',
      one: '1 car',
      other: '$volumn cars',
      name: 'textCarVolume',
      desc: '',
      args: [volumn],
    );
  }

  /// `Today`
  String get textToday {
    return Intl.message(
      'Today',
      name: 'textToday',
      desc: '',
      args: [],
    );
  }

  /// `Yesterday`
  String get textYesterday {
    return Intl.message(
      'Yesterday',
      name: 'textYesterday',
      desc: '',
      args: [],
    );
  }

  /// `Vehicle Status for the past 24 hour`
  String get titleDashboardChart {
    return Intl.message(
      'Vehicle Status for the past 24 hour',
      name: 'titleDashboardChart',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get titlePolicy {
    return Intl.message(
      'Privacy Policy',
      name: 'titlePolicy',
      desc: '',
      args: [],
    );
  }

  /// `Terms of Service`
  String get titleTerms {
    return Intl.message(
      'Terms of Service',
      name: 'titleTerms',
      desc: '',
      args: [],
    );
  }

  /// `User need to accept the "Privacy Policy" before continuing login.`
  String get warnConsentDenyMsg {
    return Intl.message(
      'User need to accept the "Privacy Policy" before continuing login.',
      name: 'warnConsentDenyMsg',
      desc: '',
      args: [],
    );
  }

  /// `Login is Denined`
  String get warnPolicyDenyTitle {
    return Intl.message(
      'Login is Denined',
      name: 'warnPolicyDenyTitle',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'th'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
