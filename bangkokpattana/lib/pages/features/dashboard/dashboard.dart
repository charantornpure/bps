
import 'package:bangkokpattana/pages/features/dashboard/bloc/dashboard_bloc.dart';
import 'package:bangkokpattana/styles/styles.dart';
import 'package:bangkokpattana/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  State<Dashboard> createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> with AutomaticKeepAliveClientMixin {
  final collapsedBarHeight = 140;
  final expandedBarHeight = 250;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final dashboardBloc = BlocProvider.of<DashboardBloc>(context);

    return BlocConsumer<DashboardBloc, DashboardState>(
      bloc: dashboardBloc..add(InitialEvent()),
      listenWhen: (previous, current) => current is! InitialState,
      buildWhen: (previous, current) => current is PartMainState,
      listener: (context, state) {
        if (state is ErrorNoUserId) {
          context.goNamed('Login');
        }
      },
      builder: (context, state) {
        return SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              //* แสดง Card สถานะรถ
              BlocBuilder(
                bloc: dashboardBloc,
                buildWhen: (previous, current) => current is PartStatusState,
                builder: (context, state) {
                  /// แสดงเมื่อข้อมูลพร้อมแสดง

                  return Container(
                    child: Text('หน้า Home'),
                  );

                  /// แสดงเมื่ออยู่ในสถานะโหลด
                },
              ),

              Container(
                padding: const EdgeInsets.all(15),
                height: MediaQuery.of(context).size.height,
                color: EzvColors.neutral100,
                child: BlocBuilder(
                  bloc: dashboardBloc,
                  buildWhen: (previous, current) =>
                      current is PartPercentageState,
                  builder: (context, state) {
                    return EzvIconButton(
                      icon: Icons.keyboard_double_arrow_right,
                      backgroundColor: EzvColors.neutral100,
                      color: EzvColors.neutral70,
                      size: 25,
                      splashColor: EzvColors.primary50,
                      onTap: () {
                        //! Logout but,if removeData user can't go to login page
                        // Preferences.removeData('USER_ID');
                        // Preferences.removeData('USERNAME');
                        // Preferences.removeData('PASSWORD');
                        // Preferences.removeData('REMEMBER_ME');
                        context.goNamed('Login');
                      },
                    );
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
