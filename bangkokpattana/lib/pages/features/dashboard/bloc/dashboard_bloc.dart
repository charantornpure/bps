// dart packages
// bloc part
import 'package:bloc/bloc.dart';
// project packages

// project models

import 'package:bangkokpattana/utils/format_locale.dart';
// flutter packages
import 'package:bangkokpattana/utils/storage_preferences.dart';
import 'package:bangkokpattana/utils/storage_secure.dart';
import 'package:flutter/material.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  DashboardBloc() : super(InitialState()) {
    //* [InitialEvent]
    on<InitialEvent>((event, emit) async {
      final String locale = await Preferences.readData(Type.string, 'LOCALE');
      final String userId = await SecureStorage.readData('USER_ID') ?? '';

      /// ถ้าไม่มีข้อมูล User id ใน Local storage จะแสดง Error dialog และไปยังหน้าล็อคอิน
      /// แก้ปัญหากรณีผู้ใช้เข้าถึงหน้านี้นอกจากผ่านการล็อคอิน เช่น เข้าถึงจาก Deep link โดยไม่ผ่านการล็อคอิน
      if (userId.isEmpty) {
        emit(ErrorNoUserId());
        return;
      }

      /// เข้าถึงหน้านี้ในรูปแบบปกติ พร้อมกับโหลดข้อมูล Percentage และ Status มาแสดงใน Widget
      else {
        emit(ReadyState(locale: LocaleFormat.stringToLocale(locale)));


      }
    });

   
  }
}
