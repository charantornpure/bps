part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardEvent {}

/// Event ที่ทำงานเป็นอันดับแรก เมื่อมีการสร้าง Bloc Provider ขึ้น
class InitialEvent extends DashboardEvent {
  final String? userId;
  final String? displayName;

  InitialEvent({
    this.userId,
    this.displayName,
  });
}

/// Event ที่ดึงข้อมูลมาจาก DashboardMobileStatus API
/// นำมาเก็บไว้เป็น [DataStatus] Model และส่งออก
class FetchStatusEvent extends DashboardEvent {
  final String userId;

  FetchStatusEvent({
    required this.userId,
  });
}

/// Event ที่ดึงข้อมูลมาจาก DashboardMobilePercentage API
/// นำมาเก็บไว้เป็น Array ของ [DataPercentage] Model และส่งออก
class FetchPercentageEvent extends DashboardEvent {
  final String userId;

  FetchPercentageEvent({
    required this.userId,
  });
}

abstract class CircleEvent {}

class LoadCircleEvent extends CircleEvent {}
