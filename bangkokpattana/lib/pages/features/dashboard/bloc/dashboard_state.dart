part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardState {}

class InitialState extends DashboardState {}

abstract class PartMainState extends DashboardState {}

class ReadyState extends PartMainState {
  final Locale locale;
  ReadyState({required this.locale});
}

abstract class PartStatusState extends DashboardState {}

class LoadingStatus extends PartStatusState {}

abstract class PartPercentageState extends DashboardState {}

class LoadingPercentage extends PartPercentageState {}


abstract class ShowDialogState extends DashboardState {}

class ErrorNoUserId extends ShowDialogState {}

class ErrorServerIssue extends ShowDialogState {}

class ErrorInternetIssue extends ShowDialogState {}

class ErrorTimeoutIssue extends ShowDialogState {}

class ErrorUnexpected extends ShowDialogState {}

abstract class CircleStatus extends DashboardState {}

class CircleStatusInitialState extends CircleStatus {}

class CircleStatusErrorState extends CircleStatus {
  final String error;

  CircleStatusErrorState({required this.error});
}
