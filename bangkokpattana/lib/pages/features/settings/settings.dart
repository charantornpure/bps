import 'package:bangkokpattana/pages/login/widgets/widgets.dart';
import 'package:bangkokpattana/styles/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../widgets/widgets.dart';
import 'bloc/settings_bloc.dart';

typedef LocaleCallback = Function(Locale? locale);

class Settings extends StatefulWidget {
  // final Locale? locale;

  final LocaleCallback? onSwitchLanguage;
  const Settings({
    super.key,
    // this.locale,
    this.onSwitchLanguage,
  });

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings>
    with AutomaticKeepAliveClientMixin {
  Locale currentLocale = Locale.fromSubtags(
    languageCode: 'th',
    countryCode: 'TH',
  );
  bool alertSwitch = false;
  bool langaugeSwitch = false;
  SettingsBloc setting = SettingsBloc();

  @override
  void initState() {
    super.initState();

    setting.add(SettingInitialEvent(password: 'mpa123', username: 'mpadev1'));
  }

  Widget build(BuildContext context) {
    super.build(context);
    List<double> fontSizes = [12, 16, 20]; // รายการขนาดตัวอักษรที่เลือกได้
    double selectedFontSize = 16;
    var size = MediaQuery.of(context).size;
    int maxLine = 1;
    return BlocConsumer<SettingsBloc, SettingsState>(
      bloc: setting,
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        switch (state.status) {
          case SettingStatus.initial:
            return const CircularProgressIndicator();
          case SettingStatus.success:
            return SafeArea(
                child: SingleChildScrollView(
                    child: Column(
              children: [
                EzvLocaleSwitch(
                  defaultLocale:
                      currentLocale, // ให้ใช้ currentLocale เป็น defaultLocale
                  onChange: (Locale? locale) {
                    setState(() {
                      // ตรวจสอบ languageCode และ countryCode แล้วเปลี่ยนค่าตามต้องการ
                      if (currentLocale.languageCode == 'th' &&
                          currentLocale.countryCode == 'TH') {
                        currentLocale = Locale('en', 'EN');
                      } else {
                        currentLocale = Locale('th', 'TH');
                      }
                    });
                  },
                ),
              ],
            )));
          case SettingStatus.failure:
            return const Center(
              child: Text('you are failure'),
            );
          default:
            return Container();
        }
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
