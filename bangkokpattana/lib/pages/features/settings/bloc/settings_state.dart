part of 'settings_bloc.dart';

enum SettingStatus { initial, success, failure }

@immutable
class SettingsState {
  final SettingStatus status;
  final int userID;
  final String userName;
  final bool isTHLanguage;
  final bool notification;

  const SettingsState({
    this.status = SettingStatus.initial,
    this.userID = 0,
    this.userName = '',
    this.isTHLanguage = true,
    this.notification = true,
  });

  SettingsState copyWith({
    SettingStatus? status,
    int? userID,
    String? userName,
    bool? isTHLanguage,
    bool? notification,
  }) {
    return SettingsState(
      status: status ?? this.status,
      userID: userID ?? this.userID,
      userName: userName ?? this.userName,
      isTHLanguage: isTHLanguage ?? this.isTHLanguage,
      notification: notification ?? this.notification,
    );
  }
}
