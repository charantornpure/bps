part of 'settings_bloc.dart';

@immutable
abstract class SettingsEvent {}

class SettingInitialEvent extends SettingsEvent {
  final String username;
  final String password;

  SettingInitialEvent({required this.username, required this.password});
}

class SettingsSwitchLanguage extends SettingsEvent {}
