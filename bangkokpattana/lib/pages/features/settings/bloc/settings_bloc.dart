import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc() : super(const SettingsState()) {
    on<SettingsSwitchLanguage>(settingsSwitchLanguage);
  }



  FutureOr<void> settingsSwitchLanguage(
      SettingsSwitchLanguage event, Emitter<SettingsState> emit) {
    emit(state.copyWith(isTHLanguage: !state.isTHLanguage));
  }
}
