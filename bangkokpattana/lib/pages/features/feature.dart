
import 'package:bangkokpattana/pages/features/dashboard/dashboard.dart';
import 'package:bangkokpattana/pages/features/settings/settings.dart';
import 'package:bangkokpattana/widgets/widgets.dart';
import 'package:flutter/material.dart';



class Feature extends StatefulWidget {
  final String page;
  const Feature({super.key, required this.page});

  @override
  State<Feature> createState() => _Feature();
}

class _Feature extends State<Feature> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  /// ระบุ Page ปัจจุบัน
  int currentPageIdx = 0;

  double appbarHeight = 48;
  double navbarHeight = 56;

  /// กำหนด Page ของ Feature
  /// แสดงเป็น Bottom Navbar
  List<EzvTab> tabs = [
    EzvTab(
      label: 'Home',
      icon: Icons.dashboard,
    ),
    EzvTab(
      label: 'Transport',
      icon: Icons.school,
    ),
    EzvTab(
      label: 'Check-in',
      icon: Icons.travel_explore,
    ),
 
    EzvTab(
      label: 'More',
      icon: Icons.settings,
    ),
  ];

  @override
  void initState() {
    /// ตรวจสอบ argument ที่รับเข้ามา แล้วนำไประบุ Index ของ Page
    switch (widget.page) {
      case 'dashboard':
        currentPageIdx = 0;
        break;
      case 'tracking':
        currentPageIdx = 1;
        break;
      case 'report':
        currentPageIdx = 2;
      case 'notification':
        currentPageIdx = 3;
        break;
      case 'settings':
        currentPageIdx = 4;
    }

    /// สร้าง Controller ของ Navbar โดยกำหนด Page เริ่มต้นจาก Argument
    _tabController = TabController(
      initialIndex: currentPageIdx,
      length: tabs.length,
      vsync: this,
    );

    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
            Colors.white,
            Colors.white,
          ])),
      child: Column(
        children: [
          //* Appbar
   

          //* Body
          Expanded(
            child: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: const [
                Dashboard(),
                Settings(),
              ],
            ),
          ),

          //* Navbar
          EzvNavbar(
            height: navbarHeight,
            controller: _tabController,
            tabs: tabs,
          )
        ],
      ),
    ));
  }
}
