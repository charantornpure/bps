import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//* GoRouter
import 'package:go_router/go_router.dart';

//* Bloc
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bangkokpattana/pages/login/bloc/login_bloc.dart';

//* Project
import 'package:bangkokpattana/generated/l10n.dart';
import 'package:bangkokpattana/styles/styles.dart';
import 'package:bangkokpattana/widgets/widgets.dart';
import 'package:bangkokpattana/pages/login/widgets/widgets.dart';

typedef FunctionCallback = Function();

class Login extends StatelessWidget {
  const Login({super.key});

  //Todo: แก้ปัญหาเมื่อใช้คำสั่ง Emit bloc แล้ว Re-build ที่ส่วน Build ของ Widget
  @override
  Widget build(BuildContext context) {
    final loginBloc = BlocProvider.of<LoginBloc>(context);
    final appLocale = Localizations.localeOf(context);

    return SafeArea(
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              EzvColors.primary20,
              EzvColors.primary70,
            ],
          ),
        ),
        child: BlocConsumer<LoginBloc, LoginState>(
          bloc: loginBloc..add(InitialEvent(locale: appLocale)),
          buildWhen: (previous, current) => current is! ShowDialogState,
          listenWhen: (previous, current) => current is! InitialState,
          listener: (context, state) {
            //* [ShowConsentState] แสดง Consent และรอผู้ใช้ยอมรับข้อตกลง
            // if (state is ShowConsentState) {
            //   showDialog(
            //     context: context,
            //     barrierDismissible: false,
            //     builder: (context) => ConsentPage(
            //       locale: state.locale!,

            //       //* เมื่อผู้ใช้กดยอมรับข้อตกลง จะย้อนทวน [PressLoginEvent] อีกครั้ง
            //       //* พร้อมส่ง [acceptedConsent] = true (พร้อมเข้าสู่ระบบ)
            //       onAcceptConsent: () {
            //         Navigator.of(context).pop();
            //         loginBloc.add(
            //           GoLoginEvent(
            //             locale: state.locale!,
            //             inputUsername: state.username,
            //             inputPassword: state.password,
            //             acceptedConsent: true,
            //             rememberMe: state.rememberMe!,
            //           ),
            //         );
            //       },

            //       //* เมื่อผู้ใช้กดปิดจะเรียก [DenyConsentEvent] (แสดง Dialog)
            //       onCloseConsent: () {
            //         loginBloc.add(
            //           DenyConsentEvent(locale: state.locale!),
            //         );
            //       },
            //     ),
            //   );
            // }

            //* [LoginErrorState] เข้าสู่ระบบไม่สำเร็จ
            //* ผู้ใช้กรอกข้อมูล Username หรือ Password ไม่ถูกต้อง
            if (state is LoginErrorState) {
              showDialog(
                context: context,
                barrierDismissible: false,
                barrierColor: EzvColors.primary20.withOpacity(0.5),
                builder: (context) => EzvAlertDialog(
                  type: DialogType.error,
                  title: S.of(context).errorLoginFailTitle,
                  message: S.of(context).errorLoginFailMsg,
                  actions: [
                    //* Report button
                    AlertDialogAction(
                      flex: 6,
                      type: ActionType.secondary,
                      label: S.of(context).btnReportUs,
                      icon: Icons.build_circle,
                    ),

                    //* Try again button
                    AlertDialogAction(
                      flex: 7,
                      label: S.of(context).btnLoginAgain,
                      icon: Icons.replay_circle_filled_outlined,
                      onTap: () => Navigator.of(context).pop(),
                    ),
                  ],
                ),
              );
            }

            //* [DenyConsentState] แสดง Cancel confirming dialog
            if (state is DenyConsentState) {
              showDialog(
                context: context,
                barrierDismissible: false,
                barrierColor: EzvColors.primary20.withOpacity(0.5),
                builder: (context) => EzvAlertDialog(
                  type: DialogType.warning,
                  title: S.of(context).warnPolicyDenyTitle,
                  message: S.of(context).warnConsentDenyMsg,
                  actions: [
                    //* Exit button
                    AlertDialogAction(
                      flex: 4,
                      type: ActionType.cancel,
                      label: S.of(context).btnExit,
                      icon: Icons.cancel_outlined,
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        loginBloc.add(
                          InitialEvent(
                            locale: state.locale!,
                          ),
                        );
                      },
                    ),

                    //* Accept button
                    AlertDialogAction(
                      flex: 5,
                      label: S.of(context).btnPolicyReadAgain,
                      icon: Icons.policy_outlined,
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              );
            }

            //* [SuccessLoginState] เข้าสู่ระบบสำเร็จ ไปยังหน้า [Feature]
            if (state is SuccessLoginState) {
              context.goNamed(
                'Feature',
                queryParameters: {'page': 'dashboard'},
              );
            }
          },
          builder: (context, state) {
            //* [LoadingState] แสดง Form ในรูปแบบ Loading
            if (state is LoadingState) {
              return IgnorePointer(
                child: Localizations(
                  locale: state.locale,
                  delegates: const [
                    S.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                  ],
                  child: LoginForm(
                    initUsername: state.username,
                    initPassword: state.password,
                    locale: state.locale,
                    isLoading: true,
                  ),
                ),
              );
            }

            //* [ReadyState] แสดงเนื้อหาของ Form
            if (state is ReadyState) {
              return Localizations(
                locale: state.locale,
                delegates: const [
                  S.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                ],
                child: LoginForm(
                  locale: state.locale,
                  initUsername: state.username,

                  onSwitchLanguage: (locale) {
                    loginBloc.add(
                      SwitchLanguageEvent(
                        locale: locale,
                      ),
                    );
                  },
                  onRememberMe: (value) {
                    loginBloc.add(RememberMeEvent(
                      locale: state.locale,

                    ));
                  },
                  onTapLogin: (
                    username,
                    password,
                    rememberMe,
                    acceptedConsent,
                  ) {
                    loginBloc.add(
                      GoLoginEvent(
                        locale: state.locale,
                        inputUsername: username,
                        inputPassword: password,
                      ),
                    );
                  },
                ),
              );
            }

            //* แสดง Widget ว่างเปล่าเมื่อไม่ได้อยู่ใน State ที่กำหนด
            //* [LoadingState], [ReadyState]
            return SizedBox.fromSize();
          },
        ),
      ),
    );
  }
}
