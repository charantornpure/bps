import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  int a;
  String b;

  UserModel({
    required this.a,
    required this.b,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    a: json["a"],
    b: json["b"],
  );

  Map<String, dynamic> toJson() => {
    "a": a,
    "b": b,
  };
}
