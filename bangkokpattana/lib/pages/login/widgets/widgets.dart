library login_widget;

import 'package:bangkokpattana/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'package:bangkokpattana/widgets/widgets.dart';
import 'package:bangkokpattana/styles/styles.dart';

part 'consent_page.dart';
part 'form_login.dart';

typedef BoolCallback = Function(bool? value);
typedef LocaleCallback = Function(Locale locale);

typedef LoginCallback = Function(
  String username,
  String password,
  bool rememberMe,
  bool acceptedLogin,
);
