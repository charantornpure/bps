part of login_widget;

class LoginForm extends StatefulWidget {
  final Locale locale;
  final bool isLoading;
  final String? initUsername;
  final String? initPassword;
  final bool? initRememberMe;
  final bool? initAcceptedConsent;
  final LocaleCallback? onSwitchLanguage;
  final LoginCallback? onTapLogin;
  final BoolCallback? onRememberMe;
  final FunctionCallback? onAcceptConsent;
  final FunctionCallback? onCloseConsent;
  const LoginForm({
    required this.locale,
    super.key,
    this.initUsername,
    this.initPassword,
    this.initRememberMe,
    this.initAcceptedConsent,
    this.isLoading = false,
    this.onSwitchLanguage,
    this.onTapLogin,
    this.onRememberMe,
    this.onAcceptConsent,
    this.onCloseConsent,
  });

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _userController = TextEditingController();
  final _pswController = TextEditingController();
  final _formLoginKey = GlobalKey<FormState>();
  final _formValid = RegExp('[a-zA-Z0-9!#\$&*+-._~]');
  final _userFocus = FocusNode();
  final _pswFocus = FocusNode();

  String _invalidWarning = "";

  bool _passwordVisible = true;
  bool _focusInput = false;
  bool _userError = false;
  bool _pswError = false;
  bool _rememberMe = true;

  bool _acceptedConsent = false;

  @override
  void initState() {
    initInput();
    super.initState();
  }

  void initInput() {
    _rememberMe = widget.initRememberMe ?? true;
    _acceptedConsent = widget.initAcceptedConsent ?? false;
    _userController.text = widget.initUsername ?? '';
    _pswController.text = widget.initPassword ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formLoginKey,
      child: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
          margin: EdgeInsets.only(
            left: 22,
            right: 22,
            bottom: _focusInput
                ? MediaQuery.of(context).viewInsets.bottom * 0.3
                : 0,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: Colors.white.withOpacity(0.85),
          ),
          child: Focus(
            // minimize login panel while keyboard showing
            // work when user focus text input
            // TODO: should be improved
            onFocusChange: (focused) {
              if (focused) _focusInput = true;
              if (!focused) _focusInput = false;
              setState(() {});
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Title & Logo
                Text(
                  //TODO: should change to be logo
                  'bangkokpattana',
                  style: EzvTextStyles.displayMedium.copyWith(
                    color: EzvColors.primary30,
                  ),
                ),

                const SizedBox(height: 18),

                // username text input
                EzvTextField(
                  controller: _userController,
                  disabled: widget.isLoading,
                  focusNode: _userFocus,
                  icon: Icons.person,
                  isError: _userError,
                  hintText: S.of(context).holdUsername,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(16),
                    FilteringTextInputFormatter.allow(_formValid),
                  ],
                  onChange: (value) {
                    _invalidWarning = "";
                    _userError = false;
                    setState(() {});
                  },
                ),
                const SizedBox(height: 18),

                // password text input
                EzvTextField(
                  controller: _pswController,
                  disabled: widget.isLoading,
                  focusNode: _pswFocus,
                  icon: Icons.key,
                  obscureText: _passwordVisible,
                  isError: _pswError,
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                    child: Icon(_passwordVisible
                        ? Icons.visibility_off
                        : Icons.visibility),
                  ),
                  hintText: S.of(context).holdPassword,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(16),
                    FilteringTextInputFormatter.allow(_formValid),
                  ],
                  onChange: (_) {
                    _invalidWarning = "";
                    _pswError = false;
                    setState(() {});
                  },
                ),

                // Invalid warning display
                _invalidWarning.isNotEmpty
                    ? Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                            color: EzvColors.error70,
                            borderRadius: BorderRadius.circular(15)),
                        margin: const EdgeInsets.only(top: 10),
                        child: Text(
                          _invalidWarning,
                          style: EzvTextStyles.bodySmall
                              .copyWith(color: EzvColors.neutral100),
                        ))
                    : const SizedBox(height: 15),
                // Optional actions
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Remember me checkbox
                      EzvCheckbox(
                          title: S.of(context).checkRememberMe,
                          isChecked: _rememberMe,
                          onChange: (value) {
                            _rememberMe = value!;
                            widget.onRememberMe != null
                                ? widget.onRememberMe!(value)
                                : null;
                          }),
                      // Change language switch between Thai and Eng
                      EzvLocaleSwitch(
                        defaultLocale: widget.locale,
                        onChange: (locale) {
                          widget.onSwitchLanguage != null
                              ? widget.onSwitchLanguage!(locale!)
                              : null;
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 18),

                // Login button
                // Check form validation
                EzvButton(
                  label: S.of(context).btnLogin,
                  width: 220,
                  loadingLabel: S.of(context).btnLoginLoading,
                  buttonSize: ButtonSize.large,
                  color: EzvColors.primary30,
                  splashColor: EzvColors.primary70,
                  isLoading: widget.isLoading,
                  disabled: _userController.text.length < 6 ||
                      _pswController.text.length < 6 ||
                      widget.isLoading,
                  onTap: login,
                  // Validation when tap disabled login button
                  disabledOnTap: loginDisabled,
                ),

                const SizedBox(height: 15),
                // Read privacy policy button
                EzvButton(
                  label: S.of(context).btnPolicyRead,
                  disabledLabel: S.of(context).btnPolicyDone,
                  width: 220,
                  buttonSize: ButtonSize.medium,
                  color: EzvColors.secondary40,
                  splashColor: EzvColors.secondary70,
                  disabled: _acceptedConsent,
                  onTap: readPrivacyConsent,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void readPrivacyConsent() {
    FocusManager.instance.primaryFocus?.unfocus();
    showDialog(
      context: context,
      builder: (context) => ConsentPage(
        locale: widget.locale,
        onAcceptConsent: () {
          _acceptedConsent = true;

          widget.onAcceptConsent;
          Navigator.of(context).pop();
          setState(() {});
        },
        onCloseConsent: () {
          widget.onCloseConsent;
          Navigator.of(context).pop();
        },
      ),
    );
  }

  void login() {
    FocusManager.instance.primaryFocus?.unfocus();
    widget.onTapLogin != null
        ? widget.onTapLogin!(
            _userController.text,
            _pswController.text,
            _rememberMe,
            _acceptedConsent,
          )
        : null;
  }

  void loginDisabled() {
    // Username and password are empty
    if (_userController.text.isEmpty && _pswController.text.isEmpty) {
      _userError = true;
      _pswError = true;
      _invalidWarning = S.of(context).invalidEmptyUser;
      FocusScope.of(context).requestFocus(_userFocus);

      // Username is empty
    } else if (_userController.text.isEmpty) {
      _userError = true;
      _invalidWarning = S.of(context).invalidEmptyUser;
      FocusScope.of(context).requestFocus(_userFocus);

      // Password is empty
    } else if (_pswController.text.isEmpty) {
      _pswError = true;
      _invalidWarning = S.of(context).invalidEmptyPsw;
      FocusScope.of(context).requestFocus(_pswFocus);

      // Username less than 6 characters
    } else if (_userController.text.length < 6) {
      _userError = true;
      _invalidWarning = S.of(context).invalidUserLessThan(6);
      FocusScope.of(context).requestFocus(_userFocus);

      // Password less than 6 characters
    } else if (_pswController.text.length < 6) {
      _pswError = true;
      _invalidWarning = S.of(context).invalidPswLessThan(6);
      FocusScope.of(context).requestFocus(_pswFocus);
    }
    setState(() {});
  }
}
