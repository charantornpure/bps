part of login_widget;

class ConsentPage extends StatefulWidget {
  /// กำหนด Locale ของ Webview
  final Locale locale;

  /// กำหนดคำสั่งเมื่อ ยอมรับเงื่อนไข
  final FunctionCallback onAcceptConsent;

  /// กำหนดคำสั่งเมื่อ กดปิดหน้า Webview
  final FunctionCallback onCloseConsent;

  const ConsentPage({
    super.key,
    required this.locale,
    required this.onAcceptConsent,
    required this.onCloseConsent,
  });

  @override
  State<ConsentPage> createState() => _ConsentPageState();
}

//Todo: แก้ปัญหาการทำงานหนักบน Thread
//Todo: อาจจะเปลี่ยนจาก [StatefulWidget] เป็น [HookWidget]
class _ConsentPageState extends State<ConsentPage> {
  late final WebViewController _webViewController;

  bool _readStart = true;
  bool _readEnd = false;

  @override
  void initState() {
    initWebview();
    super.initState();
  }

  void initWebview() {
    _webViewController = WebViewController()

      //* เปิดใช้งาน JavaScript ใน Webview
      ..setJavaScriptMode(JavaScriptMode.unrestricted)

      //* สร้าง Channel สำหรับการอ่าน JavaScript ผ่าน Webview
      ..addJavaScriptChannel('FLUTTER_CHANNEL', onMessageReceived: (message) {
        //* ทำงานเมื่อตำแหน่งปัจจุบันอยู่บนสุดของ Webview
        if (message.message.toString() == 'START_SCROLL') {
          _readStart = true;
        }

        //* ทำงานเมื่อตำแหน่งปัจจุบัน ไม่ได้ อยู่บนสุดของ Webview
        else if (message.message.toString() == 'NOT_START_SCROLL') {
          _readStart = false;
        }

        //* ทำงานเมื่อตำแหน่งปัจจุบันอยู่ท้ายสุดของ Webview
        if (message.message.toString() == 'END_SCROLL') {
          _readEnd = true;
        }

        //* ทำงานเมื่อตำแหน่งปัจจุบัน ไม่ได้ อยู่ท้ายสุดของ Webview
        else if (message.message.toString() == 'NOT_END_SCROLL') {
          _readEnd = false;
        }

        setState(() {});
      })

      //* โหลด Webview จากไฟล์ html ในโฟลเดอร์ assets
      ..loadFlutterAsset(
          'assets/html/consent-${widget.locale.languageCode}.html');
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.onCloseConsent;
        return true;
      },
      child: Dialog(
        insetPadding: const EdgeInsets.all(0),
        child: Stack(
          alignment: Alignment.center,
          children: [
            //* ส่วนของ Webview
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: WebViewWidget(
                controller: _webViewController,
              ),
            ),

            //* Button ยอมรับข้อตกลง ซึ่งจะแสดง เมื่อ Scroll ไปยังตำแหน่ง End ของ Webview
            Positioned(
              bottom: 15,
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                reverseDuration: const Duration(milliseconds: 150),
                child: _readEnd
                    ? EzvButton(
                        icon: Icons.check,
                        width: 220,
                        buttonSize: ButtonSize.large,
                        color: EzvColors.secondary40,
                        splashColor: EzvColors.secondary60,
                        label: S.of(context).btnPolicyAccept,
                        onTap: widget.onAcceptConsent,
                      )
                    : SizedBox.fromSize(),
              ),
            ),

            //* แสดง ฺButton ปิดหน้า Webview เมื่ออยู่ตำแหน่ง Start ของ Webview
            //* แสดง Button ที่ Scroll ไปยังตำแหน่ง Start เมื่อตำแหน่งปัจจุบันไม่ใช่ Start
            Positioned(
              top: 15,
              right: 15,
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                reverseDuration: const Duration(milliseconds: 150),
                child: _readStart
                    ? EzvIconButton(
                        backgroundColor: EzvColors.error70,
                        splashColor: EzvColors.error90,
                        color: EzvColors.neutral100,
                        icon: Icons.close,
                        size: 28,
                        onTap: widget.onCloseConsent,
                      )
                    : EzvIconButton(
                        backgroundColor: EzvColors.neutral20.withOpacity(0.45),
                        color: EzvColors.neutral100,
                        icon: Icons.arrow_upward,
                        size: 28,
                        onTap: () {
                          _webViewController.runJavaScript(
                              "window.scrollTo({top: 0, behavior: 'smooth'});");
                        },
                      ),
              ),
            ),

            //* แสดง Button ที่ Scroll ไปยังตำแหน่ง End เมื่อตำแหน่งปัจจุบันไม่ใช่ End
            Positioned(
              bottom: 15,
              right: 15,
              child: !_readEnd
                  ? AnimatedSwitcher(
                      duration: const Duration(milliseconds: 150),
                      reverseDuration: const Duration(milliseconds: 150),
                      child: EzvIconButton(
                        backgroundColor: EzvColors.neutral20.withOpacity(0.45),
                        color: EzvColors.neutral100,
                        icon: Icons.arrow_downward,
                        size: 28,
                        onTap: () {
                          _webViewController.runJavaScript(
                              "window.scrollTo({top: document.body.offsetHeight, behavior: 'smooth'});");
                        },
                      ),
                    )
                  : SizedBox.fromSize(),
            )
          ],
        ),
      ),
    );
  }
}
