part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class InitialState extends LoginState {}

class SuccessLoginState extends LoginState {}

/// Form changed states
abstract class FormChangeState extends LoginState {
  final Locale locale;
  final String? username;
  final String? password;

  FormChangeState({
    required this.locale,
    this.username,
    this.password,

  });
}

class ReadyState extends FormChangeState {
  ReadyState({
    required super.locale,
    required super.username,

  });
}

class LoadingState extends FormChangeState {
  LoadingState({
    required super.locale,
    required super.username,
    required super.password,

  });
}

//* [ShowDialogState] แสดง Dialog โดยที่ไม่มีการ Re-build
abstract class ShowDialogState extends LoginState {
  final Locale? locale;
  final String? title;
  final String? message;
  final String? devMessage;

  ShowDialogState({
    required this.locale,
    this.title,
    this.message,
    this.devMessage,
  });
}

class LoginErrorState extends ShowDialogState {
  LoginErrorState({
    required super.locale,
    required super.title,
    required super.message,
    required super.devMessage,
  });
}

class ServerErrorState extends ShowDialogState {
  ServerErrorState({
    required super.locale,
    required super.title,
    required super.message,
    required super.devMessage,
  });
}

class InternetErrorState extends ShowDialogState {
  InternetErrorState({
    required super.locale,
    required super.title,
    required super.message,
    required super.devMessage,
  });
}

class TimeoutErrorState extends ShowDialogState {
  TimeoutErrorState({
    required super.locale,
    required super.title,
    required super.message,
    required super.devMessage,
  });
}

class UnexpectedError extends ShowDialogState {
  UnexpectedError({
    required super.locale,
    required super.title,
    required super.message,
    required super.devMessage,
  });
}

class DenyConsentState extends ShowDialogState {
  DenyConsentState({
    required super.locale,
  });
}

class ShowConsentState extends ShowDialogState {
  final String? username;
  final String? password;
  final String? displayName;


  ShowConsentState({
    required super.locale,
    this.username,
    this.password,
    this.displayName,

  });
}
