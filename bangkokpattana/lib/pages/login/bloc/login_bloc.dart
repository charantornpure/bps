//* flutter packages
import 'package:bangkokpattana/utils/http_fetch_data.dart';
import 'package:bangkokpattana/utils/storage_secure.dart';
import 'package:flutter/material.dart';

//* project packages
import 'package:bangkokpattana/utils/storage_preferences.dart';
import 'package:bangkokpattana/utils/format_locale.dart';

//* bloc part
import 'package:bloc/bloc.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(InitialState()) {
    //* [InitialEvent] ทำงานครั้งแรก
    on<InitialEvent>((event, emit) async {
      // อ่านข้อมูล Username ใน Local storage เพื่อกำหนด Username เริ่มต้นของ Form
      String initUsername = await Preferences.readData(Type.string, 'USERNAME');

      emit(ReadyState(
        /// หากไม่มีข้อมูลใน Local storage จะกำหนดเป็นค่าว่างสำหรับ Username form
        username: initUsername,

        /// กำหนดค่า [locale] เพื่อแสดงภาษาสำหรับหน้า [login]
        locale: event.locale,

        /// กำหนดค่าเริ่มต้นของ [rememberMe] กรณีผู้ใช้ลืมติ๊ก

      ));
    });

    //* [GoLoginEvent] ทำงานเมื่อผู้ใช้เข้าสู่ระบบ รวมถึงการเข้าสู่ระบบอัติโนมัติ
    on<GoLoginEvent>((event, emit) async {
      emit(LoadingState(
        locale: event.locale,
        username: event.inputUsername,
        password: event.inputPassword,

      ));

      try {
        final data =
            await FetchData.getUser(event.inputUsername!, event.inputPassword!);

        /// ถ้าผู้ใช้กดยอมรับนโยบายแล้ว จะดำเนินเข้าสู่ระบบต่อไป
        if (1==1) {
          SecureStorage.writeData('USERNAME', event.inputUsername!);
          SecureStorage.writeData('USER_ID', data['a'].toString());
          SecureStorage.writeData('USER_DISPLAY', data['b']);
          Preferences.writeData(
              Type.string, 'LOCALE', LocaleFormat.localeToString(event.locale));

          /// ถ้าผู้ใช้ติ๊ก Remember Me ด้วย จำจดจำรหัสผ่าน และเข้าสู่ระบบอัติโนมัติ เมื่อเปิด App
          if (1==1) {
            SecureStorage.writeData('PASSWORD', event.inputPassword!);

          }

          emit(SuccessLoginState());
        }

        /// ถ้าผู้ใช้ไม่ได้กดยอมรับนโยบาย จะแสดงหน้า Consent อีกครั้ง
        else {
          emit(ShowConsentState(
            locale: event.locale,
            username: event.inputUsername,
            password: event.inputPassword,
    
          ));
        }
      }
      //* กรณีการเข้าสู่ระบบมีปัญหา
      on FetchDataException catch (e) {
        print('test');
        switch (e.type) {
          /// ไม่พบข้อมูล User
          case FetchErrorType.login:
            emit(LoginErrorState(
              locale: event.locale,
              title: e.title,
              message: e.userMsg,
              devMessage: e.secretMsg,
            ));

            break;

          /// หมดเวลาการเชื่อมต่อ (เชื่อมต่อนานเกินไป)
          case FetchErrorType.timeout:
            emit(TimeoutErrorState(
              locale: event.locale,
              title: e.title,
              message: e.userMsg,
              devMessage: e.secretMsg,
            ));
            break;

          /// มีปัญหาเกี่ยวกับ Internet
          case FetchErrorType.internet:
            emit(InternetErrorState(
              locale: event.locale,
              title: e.title,
              message: e.userMsg,
              devMessage: e.secretMsg,
            ));

          /// มีปัญหาที่ Server
          case FetchErrorType.server:
            emit(ServerErrorState(
              locale: event.locale,
              title: e.title,
              message: e.userMsg,
              devMessage: e.secretMsg,
            ));
            break;

          /// ข้อผิดพลาดที่คาดไม่ถึง
          default:
            emit(UnexpectedError(
              locale: event.locale,
              title: e.title,
              message: e.userMsg,
              devMessage: e.secretMsg,
            ));
        }
        emit(ReadyState(
          locale: event.locale,
          username: event.inputUsername,
        ));
      }
    });

    //* [RememberMeEvent] ทำงานเมื่อผู้ใช้การกด Remember Me
    on<RememberMeEvent>((event, emit) {
      emit(ReadyState(
        locale: event.locale,
        username: event.inputUsername,
      ));
    });

    //Todo: แก้ปัญหาเมื่อมีการโหลดเกิดขึ้นใน Event นี้
    //* [SwitchLanguageEvent] ทำงานเมื่อมีการกดเปลี่ยนภาษา
    on<SwitchLanguageEvent>((event, emit) async {
      /// แสดงรูปแบบ Loading ขณะเปลี่ยนภาษา (โดยปกติถูกข้าม เพราะไม่มี Delay ในการเปลี่ยนภาษา)
      emit(LoadingState(
        locale: event.locale,
        username: event.inputUsername,
        password: event.inputPassword,
      ));

      /// เปลี่ยนภาษา และเก็บข้อมูลเดิมไว้
      emit(ReadyState(
        username: event.inputUsername,
        locale: event.locale,
      ));

      /// Overwrite locale data in local storage
      Preferences.writeData(Type.string, 'LOCALE', event.locale.toString());
    });

    //* [DenyConsentEvent] เกิดขึ้นเมื่อผู้ใช้ปฏิเสธการยอมรับเงื่อนไข/นโยบาย
    on<DenyConsentEvent>((event, emit) {
      emit(DenyConsentState(locale: event.locale));
    });
  }
}
