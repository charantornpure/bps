part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {
  final Locale locale;
  final String? inputUsername;
  final String? inputPassword;


  const LoginEvent({
    required this.locale,
    this.inputUsername,
    this.inputPassword,

  });
}

class InitialEvent extends LoginEvent {
  const InitialEvent({required super.locale});
}

class GoLoginEvent extends LoginEvent {
  const GoLoginEvent({
    required super.locale,
    required super.inputUsername,
    required super.inputPassword,

  });
}

class LoginWithConsentEvent extends LoginEvent {
  const LoginWithConsentEvent({
    required super.locale,
    required super.inputUsername,

  });
}

class SwitchLanguageEvent extends LoginEvent {
  const SwitchLanguageEvent({
    required super.locale,
  });
}

class RememberMeEvent extends LoginEvent {
  const RememberMeEvent({
    required super.locale,

  });
}

class ReadConsentEvent extends LoginEvent {
  const ReadConsentEvent({
    required super.locale,
  });
}

class DenyConsentEvent extends LoginEvent {
  const DenyConsentEvent({
    required super.locale,
  });
}
