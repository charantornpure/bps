part of ezv_widget;

class AppbarAction {
  final IconData icon;
  final FunctionCallback onTap;

  const AppbarAction({
    required this.icon,
    required this.onTap,
  });
}

class EzvAppbar extends StatelessWidget {
  final Widget? leading;
  final IconData? titleIcon;
  final List<AppbarAction>? actions;
  final String titleText;
  final Color contentColor;
  final Color backgroundColor;
  final double height;
  const EzvAppbar({
    super.key,
    this.leading,
    this.titleIcon,
    this.actions,
    required this.titleText,
    this.contentColor = EzvColors.primary90,
    this.backgroundColor = EzvColors.primary40,
    required this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 48,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: backgroundColor,
          border: const Border(
              bottom: BorderSide(
            color: EzvColors.primary50,
            width: 1,
          )),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 3),
              blurRadius: 3,
              color: EzvColors.neutral0.withOpacity(0.25),
            )
          ],
        ),
        child: Stack(
          children: [
            // Appbar leading
            Align(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                width: 70,
                child: leading,
              ),
            ),

            // Appbar title
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  titleIcon != null
                      ? Container(
                          margin: const EdgeInsets.only(right: 5),
                          child: Icon(
                            titleIcon,
                            color: EzvColors.primary90,
                          ),
                        )
                      : SizedBox.fromSize(),
                  Text(
                    titleText,
                    style: EzvTextStyles.titleLarge
                        .copyWith(color: EzvColors.primary90),
                  )
                ],
              ),
            ),

            // Appbar actions
            Align(
                alignment: Alignment.centerRight,
                child: actions != null
                    ? Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: actions!.map((action) {
                          return Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 2,
                            ),
                            child: EzvIconButton(
                              size: 22,
                              icon: action.icon,
                              color: EzvColors.neutral100,
                              splashColor: EzvColors.primary70,
                              onTap: action.onTap,
                            ),
                          );
                        }).toList(),
                      )
                    : SizedBox.fromSize())
          ],
        ));
  }
}
