part of ezv_widget;

enum ButtonSize { small, medium, large }

class EzvButton extends StatelessWidget {
  /// กำหนดป้ายกำกับที่แสดงบนปุ่ม
  final String label;

  /// กำหนดสีของ [label]
  /// หากไม่ได้กำหนด ค่า Default ของ [label] จะเป็นสีขาว หรือ [EzvColors.neutral100]
  final Color labelColor;

  /// กำหนดป้ายกำกับ ที่แสดงขณะที่ปุ่มปิดการใช้งานอยู่ ([disabled] = true)
  /// หากไม่ได้กำหนด [disabledLabel] ค่า Default จะเท่ากับ [label] ที่กำหนดไว้
  final String? disabledLabel;

  /// กำหนดป้ายกำกับ ที่แสดงขณะอยู่ในสถานะกำลังโหลด ([isLoading] = true)
  /// หากไม่ได้กำหนด [loadingLabel] ค่า Default จะเท่ากับ [label] ที่กำหนดไว้
  final String? loadingLabel;

  /// กำหนดไอคอนเป็น [IconData] ที่แสดงบนปุ่มกด
  /// หากไม่ได้กำหนด จะไม่แสดงไอคอนใด ๆ
  final IconData? icon;

  /// กำหนดความสูงของปุ่มแบบกำหนดเอง
  final double? height;

  /// กำหนดความกว้างของปุ่มแบบกำหนดเอง
  final double? width;

  /// กำหนดขนาดของปุ่ม โดยอ้างอิงจาก [ButtonSize]
  /// หากกำหนด [height] ขนาดความสูงของปุ่มจะไม่มีผลจาก [ButtonSize]
  /// [ButtonSize] < [height]
  final ButtonSize buttonSize;

  /// กำหนดสีพื้นหลังของปุ่มกด โดยมีค่า Default เป็น [EzvColors.primary30]
  final Color color;

  /// กำหนดสีของ Splash ขณะที่สัมผัสปุ่ม โดยมีค่า Default เป็น [EzvColors.neutral80]
  final Color splashColor;

  /// กำหนดความโค้งที่มุมของปุ่ม อ้างอิงตามรัศมีของวงกลม
  final double radius;

  /// กำหนดค่าความหนาของเงา ที่กระทบของปุ่ม
  final double elevation;

  /// กำหนดให้ปุ่มปิดการใช้งาน ซึ่งจะไม่สามารถ Interact ได้
  final bool disabled;

  /// กำหนดให้ปุ่มอยู่ในสถานะกำลังโหลด ซึ่งจะแสดงเอฟเฟค [Shimmer]
  final bool isLoading;

  /// กำหนดคำสั่ง เมื่อปุ่มถูกกด
  final FunctionCallback onTap;

  /// กำหนดคำสั่ง เมื่อปุ่มถูกกด ขณะที่ปุ่มอยู่ในสถานะ [disabled]
  final FunctionCallback? disabledOnTap;

  const EzvButton({
    super.key,
    required this.label,
    this.disabledLabel,
    this.loadingLabel,
    this.icon,
    this.width,
    this.height,
    this.labelColor = EzvColors.neutral100,
    this.buttonSize = ButtonSize.medium,
    this.color = EzvColors.neutral10,
    this.splashColor = EzvColors.neutral80,
    this.radius = 30.0,
    this.elevation = 3.0,
    this.disabled = false,
    this.isLoading = false,
    required this.onTap,
    this.disabledOnTap,
  });

  @override
  Widget build(BuildContext context) {
    late TextStyle textStyle;
    late double iconSize;
    late double finalHeight;
    String finalLabel = label;

    switch (buttonSize) {
      case ButtonSize.small:
        finalHeight = height ?? 32;
        textStyle = EzvTextStyles.bodySmall;
        iconSize = 18;
      case ButtonSize.medium:
        finalHeight = height ?? 38;
        textStyle = EzvTextStyles.bodyMedium;
        iconSize = 22;
      case ButtonSize.large:
        finalHeight = height ?? 46;
        textStyle = EzvTextStyles.bodyLarge;
        iconSize = 24;
    }

    if (isLoading && loadingLabel != null) {
      finalLabel = loadingLabel!;
    } else if (disabled && disabledLabel != null) {
      finalLabel = disabledLabel!;
    }

    return Material(
      elevation: elevation,
      borderRadius: BorderRadius.circular(radius),
      type: MaterialType.transparency,
      child: disabled
          ? GestureDetector(
              onTap: disabledOnTap,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Shimmer.fromColors(
                    baseColor: color,
                    highlightColor: color.withOpacity(0.45),
                    enabled: isLoading,
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      width: width,
                      height: finalHeight,
                      decoration: BoxDecoration(
                        color: color.withOpacity(0.45),
                        borderRadius: BorderRadius.circular(radius),
                        border:
                            Border.all(color: color.withOpacity(0.1), width: 2),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      icon != null
                          ? Container(
                              margin: const EdgeInsets.only(right: 5),
                              child: Icon(
                                icon,
                                size: iconSize,
                                color: labelColor,
                              ))
                          : SizedBox.fromSize(),
                      Text(
                        finalLabel,
                        style: textStyle.copyWith(color: labelColor),
                      ),
                    ],
                  ),
                ],
              ),
            )
          : InkWell(
              borderRadius: BorderRadius.circular(radius),
              splashColor: splashColor,
              onTap: onTap,
              child: Ink(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                width: width,
                height: finalHeight,
                decoration: BoxDecoration(
                    color: color.withOpacity(0.95),
                    borderRadius: BorderRadius.circular(radius),
                    border: Border.all(color: color, width: 2)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    icon != null
                        ? Container(
                            margin: const EdgeInsets.only(right: 5),
                            child: Icon(
                              icon,
                              size: iconSize,
                              color: labelColor,
                            ))
                        : SizedBox.fromSize(),
                    Text(
                      label,
                      style: textStyle.copyWith(color: labelColor),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
