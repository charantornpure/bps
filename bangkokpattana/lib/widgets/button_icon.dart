part of ezv_widget;

class EzvIconButton extends StatelessWidget {
  final IconData icon;
  final double size;
  final Color color;
  final Color? backgroundColor;
  final Color splashColor;
  final FunctionCallback? onTap;
  const EzvIconButton({
    super.key,
    required this.icon,
    this.size = 24,
    this.color = EzvColors.neutral20,
    this.backgroundColor,
    this.splashColor = EzvColors.neutral80,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        splashColor: splashColor,
        borderRadius: BorderRadius.circular(size),
        onTap: onTap,
        child: Ink(
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(size),
          ),
          padding: const EdgeInsets.all(5),
          child: Icon(
            icon,
            size: size,
            color: color,
          ),
        ),
      ),
    );
  }
}
