library ezv_widget;

// flutter packages
import 'package:circle_flags/circle_flags.dart';
import 'package:flutter/material.dart';

// project packages
import 'package:bangkokpattana/styles/styles.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

part 'anchor_appbar.dart';
part 'anchor_navbar.dart';
part 'button_default.dart';
part 'button_icon.dart';
part 'checkbox.dart';
part 'dialog_alert.dart';
part 'input_text.dart';
part 'switch_locale.dart';

typedef FunctionCallback = Function()?;
typedef StringCallback = Function(String? value);
typedef IntCallback = Function(int? index);
typedef BoolCallback = Function(bool? value);
typedef LocaleCallback = Function(Locale? locale);
typedef ValidatorCallback = String Function(String? validator)?;
