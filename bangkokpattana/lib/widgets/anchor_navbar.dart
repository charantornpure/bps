part of ezv_widget;

class EzvTab {
  final String label;
  final IconData icon;

  EzvTab({
    required this.label,
    required this.icon,
  });
}

class EzvNavbar extends StatefulWidget {
  final TabController controller;
  final double height;
  final List<EzvTab> tabs;

  const EzvNavbar({
    super.key,
    required this.controller,
    this.height = 56,
    required this.tabs,
  });

  @override
  State<EzvNavbar> createState() => _EzvNavbar();
}

class _EzvNavbar extends State<EzvNavbar> {
  @override
  Widget build(BuildContext context) {
    final List<EzvTab> tabs = widget.tabs;

    return Container(
      height: widget.height,
      padding: const EdgeInsets.symmetric(
        vertical: 5,
        horizontal: 15,
      ),
      decoration: BoxDecoration(color: EzvColors.neutral100, boxShadow: [
        BoxShadow(
            offset: const Offset(0, -1),
            blurRadius: 1,
            spreadRadius: 1,
            color: EzvColors.primary40.withOpacity(0.12)),
      ]),
      child: TabBar(
        controller: widget.controller,
        labelStyle: EzvTextStyles.labelSmall,
        unselectedLabelColor: EzvColors.primary60,
        labelColor: EzvColors.neutral100,
        indicator: BoxDecoration(
            color: EzvColors.primary60,
            borderRadius: BorderRadius.circular(25)),
        tabs: tabs
            .map(
              (tab) => Tab(
                text: tab.label,
                icon: Icon(tab.icon),
                iconMargin: const EdgeInsets.all(0),
              ),
            )
            .toList(),
      ),
    );
  }
}
