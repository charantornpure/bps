part of ezv_widget;

/// Language switch
class EzvLocaleSwitch extends StatefulWidget {
  /// Set default locale that show for first time
  final Locale defaultLocale;

  final LocaleCallback onChange;
  const EzvLocaleSwitch({
    super.key,
    required this.defaultLocale,
    required this.onChange,
  });

  @override
  State<EzvLocaleSwitch> createState() => _EzvLocaleSwitchState();
}

class _EzvLocaleSwitchState extends State<EzvLocaleSwitch>
    with TickerProviderStateMixin {
  late Locale currentLocale;

  List<Locale> supportedLocales = const [
    Locale.fromSubtags(languageCode: 'th', countryCode: 'TH'),
    Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
  ];

  @override
  void initState() {
    currentLocale = widget.defaultLocale;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (currentLocale == supportedLocales[0]) {
          currentLocale = supportedLocales[1];
        } else {
          currentLocale = supportedLocales[0];
        }

        // setState(() {});

        widget.onChange(currentLocale);
      },
      child: SizedBox(
        width: 56,
        height: 28,
        child: Stack(
          children: [
            /// Language label
            AnimatedContainer(
              duration: const Duration(milliseconds: 200),
              alignment: currentLocale == supportedLocales[0]
                  ? Alignment.centerRight
                  : Alignment.centerLeft,
              margin: const EdgeInsets.symmetric(vertical: 4),
              padding: const EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: EzvColors.primary70,
              ),
              child: Text(
                (currentLocale.languageCode).toUpperCase(),
                style: EzvTextStyles.labelSmall
                    .copyWith(color: EzvColors.neutral100),
              ),
            ),

            /// Locale flag
            AnimatedAlign(
              duration: const Duration(milliseconds: 150),
              alignment: currentLocale == supportedLocales[0]
                  ? Alignment.centerLeft
                  : Alignment.centerRight,
              child: Container(
                decoration: BoxDecoration(
                    color: EzvColors.neutral100,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: EzvColors.neutral0.withOpacity(0.25),
                        offset: const Offset(0, 3),
                        blurRadius: 3,
                      )
                    ]),
                padding: const EdgeInsets.all(2),
                child: CircleFlag(
                    (currentLocale.countryCode ??
                            widget.defaultLocale.countryCode)!
                        .toLowerCase(),
                    size: 24),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
