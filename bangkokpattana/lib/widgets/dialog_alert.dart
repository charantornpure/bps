part of ezv_widget;

enum DialogType {
  general,
  error,
  warning,
  success,
}

enum ActionType {
  primary,
  secondary,
  cancel,
}

class AlertDialogAction {
  final ActionType type;
  final String label;
  final int? flex;
  final IconData? icon;
  final FunctionCallback onTap;

  const AlertDialogAction({
    this.type = ActionType.primary,
    required this.label,
    this.flex,
    this.icon,
    this.onTap,
  });
}

class EzvAlertDialog extends StatefulWidget {
  final DialogType type;
  final IconData? icon;
  final String title;
  final String message;
  final List<AlertDialogAction> actions;
  const EzvAlertDialog({
    super.key,
    this.type = DialogType.general,
    this.icon,
    required this.title,
    required this.message,
    required this.actions,
  });

  @override
  State<EzvAlertDialog> createState() => _EzvAlertDialogState();
}

class _EzvAlertDialogState extends State<EzvAlertDialog> {
  Color? color;
  IconData? icon;

  @override
  void initState() {
    switch (widget.type) {
      case DialogType.general:
        color = EzvColors.neutral20;
      case DialogType.success:
        color = EzvColors.success40;
        icon = Icons.check_circle_outline;
      case DialogType.warning:
        color = EzvColors.warning60;
        icon = Icons.warning_amber_rounded;
      case DialogType.error:
        color = EzvColors.error70;
        icon = Icons.error_outline;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      backgroundColor: EzvColors.neutral100,
      child: Container(
        constraints: const BoxConstraints(maxWidth: 320),
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            RichText(
                text: TextSpan(
              children: [
                icon != null
                    ? WidgetSpan(
                        alignment: PlaceholderAlignment.bottom,
                        child: Icon(
                          icon,
                          color: color,
                          size: 24,
                        ),
                      )
                    : WidgetSpan(
                        baseline: TextBaseline.alphabetic,
                        child: SizedBox.fromSize()),
                const WidgetSpan(child: SizedBox(width: 5)),
                TextSpan(
                  text: widget.title,
                  style: EzvTextStyles.titleLarge.copyWith(color: color),
                )
              ],
            )),
            const SizedBox(height: 15),
            Text(
              widget.message,
              style:
                  EzvTextStyles.bodyMedium.copyWith(color: EzvColors.neutral20),
            ),
            const SizedBox(height: 20),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: widget.actions.map((action) {
                  late Color color;
                  late Color splashColor;
                  bool isLast = false;
                  switch (action.type) {
                    case ActionType.primary:
                      color = EzvColors.primary50;
                      splashColor = EzvColors.primary70;
                    case ActionType.secondary:
                      color = EzvColors.neutral40;
                      splashColor = EzvColors.neutral60;
                    case ActionType.cancel:
                      color = EzvColors.error70;
                      splashColor = EzvColors.error90;
                  }
                  if (widget.actions.indexOf(action) ==
                      widget.actions.length - 1) {
                    isLast = true;
                  }
                  return Expanded(
                    flex: action.flex ?? 1,
                    child: Container(
                      padding: !isLast
                          ? const EdgeInsets.only(right: 7)
                          : EdgeInsets.zero,
                      child: EzvButton(
                        label: action.label,
                        icon: action.icon,
                        color: color,
                        splashColor: splashColor,
                        buttonSize: ButtonSize.medium,
                        onTap: action.onTap,
                      ),
                    ),
                  );
                }).toList())
          ],
        ),
      ),
    );
  }
}
