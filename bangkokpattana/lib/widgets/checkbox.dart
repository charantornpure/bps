part of ezv_widget;

// Checkbox
class EzvCheckbox extends StatefulWidget {
  final String title;
  final Color color;
  final bool isChecked;
  final BoolCallback onChange;
  const EzvCheckbox({
    super.key,
    this.title = '',
    this.isChecked = false,
    this.color = EzvColors.primary30,
    required this.onChange,
  });

  @override
  State<EzvCheckbox> createState() => _EzvCheckboxState();
}

class _EzvCheckboxState extends State<EzvCheckbox> {
  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return EzvColors.neutral100;
    }
    return widget.color.withOpacity(0.6);
  }

  bool isChecked = false;

  @override
  void initState() {
    super.initState();
    isChecked = widget.isChecked;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isChecked = !isChecked;
          widget.onChange(isChecked);
        });
      },
      child: Row(
        children: [
          Checkbox(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3),
            ),
            splashRadius: 16.0,
            checkColor: EzvColors.neutral100,
            fillColor: MaterialStateProperty.resolveWith(getColor),
            value: isChecked,
            onChanged: null,
          ),
          Text(
            widget.title,
            style: EzvTextStyles.labelMedium.copyWith(
              color: widget.color,
            ),
          )
        ],
      ),
    );
  }
}
