part of ezv_widget;

class EzvTextField extends StatelessWidget {
  final TextEditingController? controller;
  final bool disabled;
  final FocusNode? focusNode;
  final IconData? icon;
  final bool? obscureText;
  final String? hintText;
  final Widget? suffixIcon;
  final ValidatorCallback? validator;
  final List<TextInputFormatter>? inputFormatters;
  final StringCallback? onChange;
  final bool isError;
  const EzvTextField({
    super.key,
    this.controller,
    this.disabled = false,
    this.focusNode,
    this.icon,
    this.obscureText,
    this.hintText,
    this.suffixIcon,
    this.validator,
    this.inputFormatters,
    this.onChange,
    this.isError = false,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Material(
          elevation: 3,
          borderRadius: BorderRadius.circular(15),
          child: TextFormField(
            focusNode: focusNode,
            inputFormatters: inputFormatters,
            enabled: !disabled,
            obscureText: obscureText ?? false,
            controller: controller,
            style: EzvTextStyles.bodyMedium.copyWith(
              color: EzvColors.primary30,
            ),
            cursorColor: EzvColors.primary70,
            decoration: InputDecoration(
              isDense: true,
              filled: true,
              fillColor: EzvColors.neutral100,
              contentPadding: EdgeInsets.only(
                left: icon != null ? 50 : 12,
                top: 12,
                right: 12,
                bottom: 12,
              ),
              suffixIcon: suffixIcon,
              suffixIconColor: EzvColors.primary40,
              errorText: isError ? '' : null,
              errorStyle: const TextStyle(height: 0),
              hintText: hintText,
              hintStyle: EzvTextStyles.bodyMedium.copyWith(
                color: EzvColors.neutral70,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: const BorderSide(
                  color: EzvColors.primary70,
                  width: 2,
                ),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide.none,
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: const BorderSide(
                  color: EzvColors.error70,
                  width: 2,
                ),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide.none,
              ),
            ),
            validator: validator,
            onChanged: onChange,
          ),
        ),
        icon != null
            ? Positioned.fill(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  alignment: Alignment.centerLeft,
                  child: CircleAvatar(
                    radius: 16,
                    backgroundColor: EzvColors.primary30,
                    child: Icon(
                      icon,
                      color: EzvColors.primary90,
                    ),
                  ),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
